import 'package:firebase_auth/firebase_auth.dart' show User;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart' show BlocProvider,BlocListener;
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_event.dart';
import 'package:nanhe/bloc/api_repository.dart';
import 'package:nanhe/bloc/api_state.dart';
import 'package:nanhe/model/user_data.dart';
import 'package:nanhe/route_generator.dart' show RoutesDirectory;
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/util/data_repository.dart' show UserDataRepository, YouTubeVideoIdRepository;

class SplashScreen extends StatefulWidget {
  const SplashScreen(this.showTutorial,{Key? key}) : super(key: key);
  final bool showTutorial;

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool debugging = false;
  late final APIBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
    Future.delayed(Duration.zero, () async {
      String youtubeId = await APIRepository().getYouTubeVideoId;
      YouTubeVideoIdRepository.init(youtubeId);
    });
    Future.delayed(const Duration(seconds: 2), () async {
      bool serviceOn = await AppUtility.enableLocationService();
      if (serviceOn) {
        _bloc.getCurrentUser();
      } else {
        AppUtility.showMessageDialog(context, '''We need the location permission so we can attach the location to each picture you upload. This helps parents.''',
          onPressed: () {
            Navigator.pop(context);
            _bloc.getCurrentUser();
          }
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocListener(
          bloc: _bloc,
          listener: (BuildContext context, state) async {
            if (state is APILoadingState) {
              if (state.event is GetCurrentUserEvent) {
                if(!debugging) {
                  AppUtility.showProgressDialog(context);
                }
              }
            }
            if (state is APIErrorState) {
              if (state.event is GetCurrentUserEvent) {
                if(!debugging) {
                  if(widget.showTutorial) {
                    Navigator.pop(context);
                    Navigator.of(context).pushReplacementNamed(RoutesDirectory.howItWorks,arguments: true);
                  } else {
                    Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.login,(_) => false);
                  }

                }
              }
            }
            if (state is APILoadedState<User>) {
              if (state.event is GetCurrentUserEvent) {
                print('uid ${state.data.uid}');
                final UserData? userdata = await APIRepository().getUserData(state.data.uid);
                if(userdata != null){
                  UserDataRepository.init(userdata);
                  if (userdata.signInMethod == 'EMAIL') {
                    if (state.data.emailVerified) {
                      if (!debugging) {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                          RoutesDirectory.volunteerCamera,
                          (route) => false,
                        );
                      }
                    } else {
                      if (!debugging) {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                          RoutesDirectory.login, (route) => false,
                          arguments: 1);
                      }
                    }
                  } else {
                    print('user signed in with social');
                    if (!debugging) {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                        RoutesDirectory.volunteerCamera,
                        (route) => false,
                        arguments: 1,
                      );
                    }
                  }
                } else {
                  Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.login, (route) => false);
                }
              }
            }
          },
          child: Center(
            child: Image.asset('images/nanheLogo.png'),
          ),
        ),
      )
    );
  }
}
