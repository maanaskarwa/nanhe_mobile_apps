import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nanhe/util/app_utility.dart';

import '../route_generator.dart' show RoutesDirectory;

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          padding: EdgeInsets.only(
            left: deviceSize.width*0.07,
            right: deviceSize.width*0.07,
            top: deviceSize.height*0.04,
          ),
          width: deviceSize.width,
          height: deviceSize.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Image.asset("images/n.png",height: 50,),
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 20),
              ),
              const Spacer(),
              Text(
                'Login',
                style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 49,),
              ),
              SizedBox(height: deviceSize.height*0.1),
              // _buildSignInWithText(),
              InkWell(
                onTap: () {
                  AppUtility.googleLogin(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Theme.of(context).colorScheme.primary,
                    gradient: const LinearGradient(
                      colors: [
                        Color(0xFFFF1493),
                        Color(0xFFC00FAE),
                        Color(0xFF800AC9),
                        Color(0xFF4005E4),
                        Color(0xFF0000FF),
                      ],
                    ),
                  ),
                  height: 50,
                  padding: const EdgeInsets.only(left: 1.5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5)
                        ),
                        height: 47,
                        width: 47,
                        padding: const EdgeInsets.all(5),

                        child: Image.asset(
                          'images/googleLogo.png',
                          height: 35,
                          width: 35,
                        ),
                      ),
                      const Spacer(),
                      Text(
                        'Login with Google',
                        style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white,fontSize: 16),
                      ),
                      const Spacer()
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              _buildSignInWithText(context),
              const SizedBox(height: 25,),
              InkWell(
                onTap: () {
                  AppUtility.facebookLogin(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Theme.of(context).colorScheme.primary,
                    gradient: const LinearGradient(
                      colors: [
                        Color(0xFFFF1493),
                        Color(0xFFC00FAE),
                        Color(0xFF800AC9),
                        Color(0xFF4005E4),
                        Color(0xFF0000FF),
                      ],
                    ),
                  ),
                  height: 50,
                  padding: const EdgeInsets.only(left: 1.5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)
                        ),
                        height: 47,
                        width: 47,
                        padding: const EdgeInsets.all(5),

                        child: const Icon(FontAwesomeIcons.facebookF,size: 35,color: Color.fromRGBO(59, 89, 152, 1),),
                      ),
                      const Spacer(),
                      Text(
                        'Login with Facebook',
                        style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white,fontSize: 16),
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
              if(Platform.isIOS)...[
                InkWell(
                  onTap: () {
                    AppUtility.loginWithApple(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Theme.of(context).colorScheme.primary,
                      gradient: const LinearGradient(
                        colors: [
                          Color(0xFFFF1493),
                          Color(0xFFC00FAE),
                          Color(0xFF800AC9),
                          Color(0xFF4005E4),
                          Color(0xFF0000FF),
                        ],
                      ),
                    ),
                    height: 50,
                    padding: const EdgeInsets.only(left: 1.5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5)
                          ),
                          height: 47,
                          width: 47,
                          padding: const EdgeInsets.all(5),

                          child: const Icon(FontAwesomeIcons.apple,size: 35,),
                        ),
                        const Spacer(),
                        Text(
                          'Login with Apple',
                          style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white,fontSize: 16),
                        ),
                        const Spacer(),
                      ],
                    ),
                  ),
                ),
              ],
              const SizedBox(height: 50,),
              _buildLoginWithEmailButton(context,deviceSize),
              const SizedBox(height: 25,)
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLoginWithEmailButton(BuildContext context,Size deviceSize) {
    return Container(
      margin: const EdgeInsets.only(
        bottom: 10,
      ),
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: () => Navigator.of(context).pushNamed(RoutesDirectory.loginIncludingEmail),
        child: RichText(
          text: TextSpan(
            text: 'Login with email instead',
            style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey[800],fontSize: 14),
          ),
        ),
      ),
    );
  }

  Widget _buildSignInWithText(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: RichText(
        text: TextSpan(
          text: 'OR',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.black,fontWeight: FontWeight.w600),
        ),
      ),
    );
  }
}