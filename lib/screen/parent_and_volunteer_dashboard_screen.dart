import 'dart:io' as platform show Platform ;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/model/user_data.dart';
import 'package:nanhe/screen/subScreen/parent_dashboard.dart';
import 'package:nanhe/screen/subScreen/volunteer_dashboard.dart';
import 'package:nanhe/util/app_constants.dart';
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/util/data_repository.dart' show UserDataRepository ;
import 'package:url_launcher/url_launcher.dart';

class PAndVDashboard extends StatefulWidget {
  const PAndVDashboard ({Key? key}) : super(key: key);

  @override
  State<PAndVDashboard> createState() => _PAndVDashboardState();
}

class _PAndVDashboardState extends State<PAndVDashboard> {
  late final APIBloc bloc;
  late final UserData userData;
  bool _userOpenedStoreForReview = false;

   @override
  void initState() {
    bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
    userData = UserDataRepository.getUserData!;
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: DefaultTabController(
        length: 2,
        child: SizedBox(
          height: deviceSize.height,
          width: deviceSize.width,
          child: Column(
            children: [
              TabBar(
                labelColor: Theme.of(context).colorScheme.primary,
                unselectedLabelColor: Colors.grey,
                indicatorColor: Theme.of(context).colorScheme.primary,
                unselectedLabelStyle: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w400,),
                labelStyle: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600,),
                tabs: const [
                  Tab(
                    text: 'Volunteer',
                  ),
                  Tab(
                    text: 'Parent',
                  )
                ],
              ),
              const Expanded(
                child: TabBarView(
                  children: [
                    VolunteerDashboard(),
                    ParentDashboard(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: userData.review != null? null :_userOpenedStoreForReview ? null : platform.Platform.isAndroid? Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 0),
          child: TextButton(
            onPressed: () async {
              if(platform.Platform.isAndroid) {
                bool canLaunchUrl = await canLaunch('https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app');
                 if(canLaunchUrl) {
                   await launch('https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app');
                   setState(() {
                     _userOpenedStoreForReview = true;
                   });
                 } else {
                   AppUtility.showMessageDialog(context, Constants.errorSomethingWentWrong);
                 }
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RichText(
                  text: TextSpan(
                    text: 'How do you like nanhe?',
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600)
                  )
                ),
                Row(
                  children: List.generate(5, (index) => const Icon(Icons.star_outline_rounded,color: Colors.amber,)) ,
                )
              ],
            ),
          ),
        ),
      ) : null,
    );
  }
}
