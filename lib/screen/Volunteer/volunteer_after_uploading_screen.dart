import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_social_content_share/flutter_social_content_share.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nanhe/route_generator.dart';
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/widgets/image_view.dart';


class VolunteerAfterUploadingScreen extends StatelessWidget {
  const VolunteerAfterUploadingScreen({Key? key, this.url}) : super(key: key) ;
  final String? url;

  // Row _buildSocialButtonRow() {
  //   return Row(
  //     children: const [
  //
  //     ],
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    // final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pushReplacementNamed(RoutesDirectory.mainDashboard,);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                  text: 'Upload Successful 👍🏽',
                  style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 27,fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                child: ImageView(url ?? '', ImageType.url,height: 200,),
                margin: const EdgeInsets.only(top: 50,bottom: 50),
                alignment: Alignment.center,
              ),
              Center(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'Motivate others now',
                    style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 27,fontWeight: FontWeight.w500),
                  ),
                ),
              ),
              /*RichText(
                text: TextSpan(
                  text: 'Spread the word',
                  style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 16,fontWeight: FontWeight.w600),
                ),
              ),*/
              Container(
                margin: const EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      onPressed: () async {
                        await FlutterSocialContentShare.share(
                          type: ShareType.facebookWithoutImage,
                          quote: "Proud to be a nanhe.org volunteer to help rescue children from begging. Here is the child I found.",
                          url: url,
                          // imageName: 'testImage',
                          // imageUrl: ''
                        );
                      },
                      icon: const Icon(
                        FontAwesomeIcons.facebook,
                        color: Color.fromRGBO(59, 89, 152, 1),
                        size: 40,
                      ),
                    ),
                    IconButton(
                      onPressed: () async {
                        await FlutterSocialContentShare.shareOnWhatsapp(
                          '0',
                          '''Hello,
I just helped rescue a child from begging 😊
  
You can do it too. It's simple. Just download the *nanhe* app and use it to upload pictures of children who are forced to beg at traffic signals. nanhe.org matches these pictures with pictures uploaded by parents of missing children, and helps the children reunite with their parents.
  
Be a *nanhe volunteer!!* 😎
  
Android: https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app
  
_Please forward this message ONLY AFTER you upload at least one child's picture_''',
                        );
                      },
                      icon: Icon(
                        FontAwesomeIcons.whatsapp,
                        color: Color.lerp(const Color.fromRGBO(37, 211, 102, 1), const Color.fromRGBO(18, 140, 126, 1), 0),
                        size: 40,
                      ),
                    ),
                    GestureDetector(
                      child: Container(
                        height: 40,
                        width: 40,
                        alignment: Alignment.center,
                        padding: const EdgeInsets.only(bottom: 2.5),
                        decoration:  BoxDecoration(
                          borderRadius: BorderRadius.circular(7.5),
                          gradient: const LinearGradient(
                              colors: [
                                Color(0xffF58529),
                                Color(0xffFEDA77),
                                Color(0xffDD2A7B),
                                Color(0xff8134AF),
                                Color(0xff515BD4),
                              ],
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight
                          ),
                        ),
                        child: const Icon(
                          FontAwesomeIcons.instagram,
                          color: Colors.white,
                          size: 35,
                        ),
                      ),
                      onTap: () async {
                        FlutterClipboard.copy("Proud to be a nanhe.org volunteer to help rescue children from begging. Here is the child I found.");
                        AppUtility.showMessageDialog(
                            context,
                            'To save your time, we have created the caption for your post. Please paste it in Instagram.',
                            onPressed: () async {
                              Navigator.of(context).pop();
                              final response = await FlutterSocialContentShare.share(
                                type: ShareType.instagramWithImageUrl,
                                imageUrl: 'https://firebasestorage.googleapis.com/v0/b/nanhe-20ea0.appspot.com/o/BG%20Img%201.jpg?alt=media&token=d35bfd18-f480-4ade-a285-c51c0e0d79c3',
                              );
                            }
                        );
                      },
                    )
                  ],
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 30),
                  alignment:Alignment.center,
                  child: TextButton(onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.mainDashboard,(_) => false), child: const Text('Back to Home'))),
            ],
          ),
        ),
      ),
    );
  }
}
