import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image/image.dart' as face_ui;
import 'package:nanhe/route_generator.dart' show RoutesDirectory;
import 'package:nanhe/util/app_utility.dart';

class CameraScreen extends StatefulWidget {
  final List<CameraDescription>? cameras;
  const CameraScreen({Key? key, required this.cameras,}) : super(key: key);

  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> with WidgetsBindingObserver
{
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if(rearController == null) {
      return;
    }
    else {
      if(!rearController!.value.isInitialized) {
        return;
      }
    }
    if (state == AppLifecycleState.inactive) {
      rearController?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      initializeRearCamera();
    }
  }

  late final CameraController? rearController;
  late final Future<void>? _initializeRearControllerFuture;
  bool _isLoading = false;

  void initializeRearCamera() {
    rearController = CameraController(
      widget.cameras!.lastWhere((element) {
        print(element.toString());
        print(element.name);
        return element.lensDirection == CameraLensDirection.back;
      },orElse: () => widget.cameras!.first),
      ResolutionPreset.high,
      enableAudio: false,
    );
    _initializeRearControllerFuture = rearController!.initialize();
  }

  @override
  void initState() {
    super.initState();
    if(widget.cameras != null) {
      if(widget.cameras!.isNotEmpty) {
        print('camera');
        print(widget.cameras);
        initializeRearCamera();
      }
    }
  }

  @override
  void dispose() {
    rearController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark
      ),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black,
          body: Stack(
            children: [
              // from camera package documentation on pub.dev
              FutureBuilder<void>(
                future: _initializeRearControllerFuture,
                builder: (context,snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Center(
                      child: CameraPreview(rearController!)
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator()
                    );
                  }
                },
              ),
              Positioned(
                child: const Text(" Volunteer's Screen ",textAlign: TextAlign.center,style: TextStyle(color: Colors.white,backgroundColor: Colors.black,)),
                top:deviceSize.height*0.055,
                right: 0,
                width: deviceSize.width/2,
              ),
              Positioned(
                child: Container(
                  width: 40,
                  height: 40,
                  padding: EdgeInsets.zero,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white
                  ),
                  alignment: Alignment.center,
                  child: IconButton(
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.mainDashboard, (route) => false);
                    },
                    icon: const Icon(Icons.home,),
                  ),
                ),
                top: deviceSize.height*0.04,
                left: deviceSize.width*0.07,
              ),
            ]
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.white,
            child: _isLoading? const CircularProgressIndicator(color: Colors.black,) : const Icon(Icons.camera_alt,size: 35,color: Colors.black,),
            onPressed: _isLoading? null : () async {
              await _initializeRearControllerFuture;
              try {
                setState(() {
                  _isLoading = true;
                });
                final image = await rearController?.takePicture();
                if(image != null) {
                  /*
                  * Face detection was previously done before uploading in APIRepository.dart
                  * However, if a face is not detected, the image is not useful for face detection. Better to do face recognition here itself.
                  * Was previously taking first face detected in image for matching... that could be the parent beggar's face too. Need to ensure it's the kid's face. Am displaying a "Select child's face" screen before upload
                  */
                  FaceDetector detector = GoogleMlKit.vision.faceDetector();
                  List<Face> faces = await detector.processImage(InputImage.fromFile(File(image.path)));
                  if(faces.isEmpty) {
                    setState(() {
                      _isLoading = false;
                    });
                    AppUtility.showMessageDialog(
                      context,
                      '''No face detected. Please try again''',
                    );
                  } else {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    List<face_ui.Image> faceImages = [];
                    for(var childFace in faces) {
                      face_ui.Image picture = face_ui.copyCrop(
                        face_ui.decodeImage(File(image.path).readAsBytesSync())!,
                        childFace.boundingBox.left.toInt(),
                        childFace.boundingBox.top.toInt(),
                        childFace.boundingBox.width.toInt(),
                        childFace.boundingBox.height.toInt(),
                      );
                      faceImages.add(picture);
                    }
                    Navigator.of(context).pushReplacementNamed(
                      // Select child's face on the below screen
                      RoutesDirectory.addChildDataAsVolunteer,
                      arguments: <String,dynamic>{
                        'image': image,
                        'faceImages':faceImages,
                      },
                    );
                  }
                } else {
                  setState(() {
                    _isLoading = false;
                  });
                }
              } catch(error) {
                print('error in camera');
                print(error);
                setState(() {
                  _isLoading = false;
                });
              }
            },
          ),
        ),
      ),
    );
  }
}