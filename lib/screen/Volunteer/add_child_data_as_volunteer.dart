import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart' show XFile;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image/image.dart' as face_ui;
import 'package:location/location.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_state.dart';
import 'package:nanhe/model/child_data_from_volunteer.dart';
import 'package:nanhe/route_generator.dart' show RoutesDirectory;
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/widgets/image_view.dart';

class AddChildDataAsVolunteerScreen extends StatefulWidget {
  const AddChildDataAsVolunteerScreen(this.childImages, {Key? key,}) : super(key: key);
  final Map<String,dynamic> childImages;

  @override
  _AddChildDataAsVolunteerScreenState createState() => _AddChildDataAsVolunteerScreenState();
}

class _AddChildDataAsVolunteerScreenState extends State<AddChildDataAsVolunteerScreen> {
  late final APIBloc bloc;
  Location? location;
  LocationData? locationData;
  List<File> imagesToUpload = [];
  late Map<String,dynamic> childImages;
  late List<face_ui.Image> faceImages;

  int? selectedItem;
  void toggleSelectedStatus(int i) {
    setState(() {
      selectedItem = i;
    });
  }

  @override
  void initState() {
    childImages = widget.childImages;
    faceImages = (childImages['faceImages'] as List<face_ui.Image>);
    bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
    location = Location.instance;
    Future.delayed(Duration.zero, () async {
      bool serviceOn = await AppUtility.enableLocationService();
      if (serviceOn) {
        location!.onLocationChanged.listen((event) {
          if (mounted) {
            locationData = event;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.black,
          actions: const [],
        ),
        body: SafeArea(
          child: BlocListener(
            bloc: bloc,
            listener: (BuildContext context, state) {
              if (state is APILoadingState) {
                AppUtility.showProgressDialog(context);
              }
              if (state is APILoadedState<ChildDataFromVolunteer>) {
                Navigator.pop(context);
                Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.volunteerAfterUpload, (_) => false ,arguments: state.data.images!.first);
              }
              if (state is APIErrorState) {
                Navigator.pop(context);
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(state.error)));
              }
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20,right: 20,bottom: 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      text: 'Picture You Uploaded',
                      style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 27,fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                      // top: 20
                      // top: 50,
                        bottom: 10
                    ),
                    alignment: Alignment.center,
                    child: ImageView(
                      (childImages['image'] as XFile).path,
                      ImageType.file,
                      fit: BoxFit.contain,
                      height: deviceSize.height/3,
                      width: 120,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    padding: const EdgeInsets.all(5),
                    width: (deviceSize.width-40),
                  ),
                  RichText(
                    text: TextSpan(
                      text: faceImages.length == 1 ? "Confirm Child's Face" : "Select Child's Face",
                      style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 24,fontWeight: FontWeight.w500),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: faceImages.map((e) {
                        int index = faceImages.indexOf(e);
                        return InkWell(
                          onTap: () => toggleSelectedStatus(index),
                          child: index != selectedItem ? Container(
                            margin: const EdgeInsets.only(top:10,bottom: 25),
                            width: deviceSize.width*0.3,
                            height: 100,
                            child: FittedBox(child: Image.memory(Uint8List.fromList(face_ui.encodePng(e)))),
                          ) : Stack(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(bottom: 25,top: 10),
                                  width: deviceSize.width*0.35,
                                  height: 120,
                                  child: FittedBox(child: Image.memory(Uint8List.fromList(face_ui.encodePng(e)))),
                                ),
                                const Positioned(
                                  top: 15,
                                  right: 5,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(Icons.check,color: Colors.green,),
                                  ),
                                ),
                              ]
                          ),
                        );
                      }).toList()
                  ),
                  Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(bottom: 45,right: 10,),
                        child: TextButton(
                          child: const Text('Retry'),
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed(RoutesDirectory.volunteerCamera);
                          },
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              )),
                              alignment: Alignment.center,
                              fixedSize: MaterialStateProperty.all<Size>(Size((deviceSize.width-60)/2,50)),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 45,left: 10),
                        child: ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              )),
                              backgroundColor: selectedItem == null ? (faceImages.length == 1 ?MaterialStateProperty.all<Color>(Theme.of(context).colorScheme.primary) : MaterialStateProperty.all<Color>(Colors.grey)) : MaterialStateProperty.all<Color>(Theme.of(context).colorScheme.primary),
                              alignment: Alignment.center,
                              fixedSize: MaterialStateProperty.all<Size>(Size((deviceSize.width-60)/2,50)),
                          ),
                          onPressed: buttonOnPressed,
                          child: Text(
                            'Upload',
                            style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white,fontSize: 16),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void buttonOnPressed() {
    if(locationData == null) {
      AppUtility.showMessageDialog(
        context,
        '''We need the location permission so we can attach the location to each picture you upload. This helps parents.''',
        onPressed: () async {
          bool serviceOn = await AppUtility.enableLocationService();
          if (serviceOn) {
            location!.onLocationChanged.listen((event) {
              if (mounted) {
                locationData = event;
              }
            });
            Navigator.of(context).pop();
          }
        }
      );
    }
    else {
      if(selectedItem == null) {
        if(faceImages.length == 1) {
          ChildDataFromVolunteer childData = ChildDataFromVolunteer(
            lat: locationData!.latitude!,
            lng: locationData!.longitude!,
            createdAt: DateTime.now(),
            idOfVolunteerWhoUploaded: FirebaseAuth.instance.currentUser!.uid,
          );
          bloc.uploadChildDataAsVolunteer(
            childImages: <String,dynamic>{
              'image': childImages['image'] as XFile,
              'faceImage': faceImages.first,
            },
            childData: childData,
          );
        } else {
          AppUtility.showMessageDialog(context, "Please select the child's image");
        }
      } else {
        ChildDataFromVolunteer childData = ChildDataFromVolunteer(
          lat: locationData!.latitude!,
          lng: locationData!.longitude!,
          createdAt: DateTime.now(),
          idOfVolunteerWhoUploaded: FirebaseAuth.instance.currentUser!.uid,
        );
        bloc.uploadChildDataAsVolunteer(
          childImages: <String,dynamic>{
            'image': childImages['image'] as XFile,
            'faceImage': faceImages[selectedItem!],
          },
          childData: childData
        );
      }
    }
  }
}

