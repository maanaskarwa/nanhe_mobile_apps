import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nanhe/screen/parent_and_volunteer_dashboard_screen.dart';
import 'package:nanhe/widgets/app_drawer.dart';

import 'home_screen.dart';
import 'login_screen.dart';

class MasterDashBoardScreen extends StatefulWidget {
  final int currentTab;

  const MasterDashBoardScreen(
    this.currentTab,
    {Key? key,}
    ) : super(key: key);

  @override
  _MasterDashBoardScreenState createState() => _MasterDashBoardScreenState();
}

class _MasterDashBoardScreenState extends State<MasterDashBoardScreen> with TickerProviderStateMixin {
  final ValueNotifier<int> _currentTabIndex = ValueNotifier(0);
  late final PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: widget.currentTab, keepPage: true);
    _currentTabIndex.value = widget.currentTab;
  }

  @override
  Widget build(BuildContext context) {
    // final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.only(left: 10, top: 2, bottom: 2),
          child: Image.asset(
            'images/n.png',
            height: 40,
            width: 40,
          ),
        ),
        centerTitle: true,
      ),
      body: FirebaseAuth.instance.currentUser == null? const LoginScreen() :
      PageView(
        controller: _pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: const [
          HomeScreen(),
          PAndVDashboard(),
        ],
      ),
      drawer: AppDrawer(CurrentScreen.none),
      bottomNavigationBar: FirebaseAuth.instance.currentUser == null? null : ValueListenableBuilder(
        builder: (context, int i, child) {
          return SizedBox(
            height: 60,
            child: BottomNavigationBar(
              showSelectedLabels: false,
              showUnselectedLabels: false,
              backgroundColor: Colors.white,
              currentIndex: _currentTabIndex.value,
              type: BottomNavigationBarType.fixed,
              selectedFontSize: 0,
              unselectedFontSize: 0,
              items: [
                BottomNavigationBarItem(
                  icon: i == 0 ? Container(
                    alignment: Alignment.center,
                    height: 30,
                    width: 120,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Theme.of(context).colorScheme.primary,
                    ),
                    child: const Text('Home',style: TextStyle(color: Colors.white,fontSize: 16),),
                  ) : Text('Home',style: TextStyle(color: Theme.of(context).colorScheme.primary),),
                  label: 'Home',
                  backgroundColor: Theme.of(context).colorScheme.primary,
                ),
                BottomNavigationBarItem(
                  icon: i == 1 ? Container(
                    alignment: Alignment.center,
                    height: 30,
                    width: 120,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Theme.of(context).colorScheme.primary,
                    ),
                    child: const Text('Children',style: TextStyle(color: Colors.white,fontSize: 16),),
                  ) : Text('Children',style: TextStyle(color: Theme.of(context).colorScheme.primary),),
                  label: 'Children',
                  backgroundColor: Theme.of(context).colorScheme.primary,
                ),
              ],
              onTap: (index) {
                _currentTabIndex.value = index;
                _pageController.jumpToPage(index);
              },
            ),
          );
        },
        valueListenable: _currentTabIndex,
      ),
    );
  }
}
