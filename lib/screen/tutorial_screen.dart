import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:introduction_screen/introduction_screen.dart';

import '../route_generator.dart';

class NanheIntroductionScreen extends StatefulWidget {
  const NanheIntroductionScreen(this.trueIfBeforeLoginScreenAndFalseIfInApp,{Key? key}) : super(key: key);
  final bool trueIfBeforeLoginScreenAndFalseIfInApp;

  @override
  _NanheIntroductionScreenState createState() => _NanheIntroductionScreenState();
}
const List<Map<String,String>> _content = [
  {
    'image' : 'images/rescue.svg',
    'title' : 'Rescue',
    'text' : '''When you see a child begging or in child labor, just take a picture on the nanhe app as a Volunteer. To report your missing child, use the Parent tab.''',
  },
  {
    'image' : 'images/reunite.svg',
    'title' : 'Reunite',
    'text' : 'We match the pictures uploaded by Volunteers and Parents and alert the parents when a match is found.',
  },
  {
    'image' : 'images/regain.svg',
    'title' : 'Regain',
    'text' : 'Parents get the child back home and the child regains the golden childhood. Mission accomplished!',
  },
];

class _NanheIntroductionScreenState extends State<NanheIntroductionScreen> {
  static Scaffold buildRawPage(BuildContext context,Size deviceSize,int i,) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: SvgPicture.asset(_content[i]['image'] as String),
              alignment: Alignment.center,
              margin: const EdgeInsets.only(bottom: 30),
              height: deviceSize.height*0.45,
            ),
            Text(
              _content[i]['title'] as String,
              style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 40,fontWeight: FontWeight.w500),
            ),
            Text(
              _content[i]['text'] as String,
              style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 14),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    List<Scaffold> rawPagesList = [
      buildRawPage(context, deviceSize, 0),
      buildRawPage(context, deviceSize, 1),
      buildRawPage(context, deviceSize, 2),
    ];
    return SafeArea(
      child: Stack(
        children: [
          IntroductionScreen(
            isProgress: true,
            rawPages: rawPagesList,
            globalBackgroundColor: Colors.white,
            done: CircleAvatar(
              backgroundColor: Theme.of(context).colorScheme.primary,
              child: const Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              radius: 30,
            ),
            onDone: () {
              if(widget.trueIfBeforeLoginScreenAndFalseIfInApp) {
                Navigator.of(context).pushReplacementNamed(RoutesDirectory.login);
              } else {
                Navigator.of(context).pop();
              }
            },
            next: CircleAvatar(
              backgroundColor: Theme.of(context).colorScheme.primary,
              child: const Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              radius: 30,
            ),

            dotsDecorator: DotsDecorator(
              activeColor: Theme.of(context).colorScheme.primary,
              size: const Size(8,8),
              activeSize: const Size(12,12),
            ),
          ),
          Positioned(
            child: GestureDetector(
              child: Text(
                'Skip',
                style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey),
              ),
              onTap: () {
                if(widget.trueIfBeforeLoginScreenAndFalseIfInApp) {
                  Navigator.of(context).pushReplacementNamed(RoutesDirectory.login);
                } else {
                  Navigator.of(context).pop();
                }
              },
            ),
            top: 20,
            right: 30,
          )
        ],
      ),
    );
  }
}
