import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_event.dart';
import 'package:nanhe/bloc/api_repository.dart';
import 'package:nanhe/bloc/api_state.dart';
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/util/data_repository.dart' show UserDataRepository ;

import '../route_generator.dart' show RoutesDirectory;

class DevLoginScreen extends StatefulWidget {
  const DevLoginScreen({Key? key}) : super(key: key);
  @override
  _DevLoginScreenState createState() => _DevLoginScreenState();
}

class _DevLoginScreenState extends State<DevLoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool _isPasswordVisible = false;
  final _formKey = GlobalKey<FormState>();

  late final APIBloc _bloc;

  String? emailErrorMessage (String? email) {
    if (email == null) {
      return 'Please enter an email address.';
    } else if (email.isEmpty) {
      return 'Please enter an email address.';
    } else if (!email.contains('@')) {
      return 'Please enter a valid email address.';
    } else if (!email.contains('.')) {
      return 'Please enter a valid email address.';
    } else {
      return null;
    }
  }

  String? passwordErrorMessage (String? password) {
    if (password == null) {
      return 'Please enter a password.';
    } else if (password.isEmpty) {
      return 'Please enter a password.';
    } else {
      return null;
    }
  }

  void _togglePasswordVisibility() {
    setState(() {
      _isPasswordVisible = !_isPasswordVisible;
    });
  }

  @override
  void initState() {
    _bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return BlocListener(
      bloc: _bloc,
      listener: (BuildContext context, state) async {
        if (state is APILoadingState) {
          AppUtility.showProgressDialog(context);
        }
        if (state is APIErrorState) {
          Navigator.pop(context);
          print(state.error);
          String errorMessage;
          if(state.error.contains('invalid-email')) {
            errorMessage = '''Invalid email.
Please login using a valid email id.''';
          } else if (state.error.contains('user-not-found')) {
            errorMessage = '''There is no user registered with this email.
Please sign up or login using a registered email.''';
          } else if (state.error.contains('wrong-password')) {
            errorMessage = '''Incorrect password provided.''';
          } else {
            errorMessage = '''An error occurred.
Please try again later.''';
          }
          AppUtility.showMessageDialog(context, errorMessage);
        }
        if (state is APILoadedState<UserCredential>) {
          print('event ${state.event}');
          if (state.event is SignInWithEmailEvent) {
            final userdata = await APIRepository().getUserData(state.data.user!.uid);
            UserDataRepository.init(userdata!);
            if (state.data.user!.emailVerified) {
              if(state.data.additionalUserInfo!.isNewUser) {
                APIRepository().incrementVolunteerCount();
                Navigator.pop(context);
                AppUtility.showMessageDialog(
                    context,
                    '''Welcome to the nanhe journey of SAVING CHILDREN 👍

Next time you see a child in begging/labor, just start the app and we'll take you directly to the camera screen so you can upload a picture quickly!!''',
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.mainDashboard, (route) => false,);
                    }
                );
              } else {
                Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.volunteerCamera, (route) => false,);
              }
            } else {
              Navigator.pop(context);
              AppUtility.showMessageDialog(context, '''A verification mail has been sent to your email address.
Please verify and log in.''');
            }
          }
        }
      },
      child: Scaffold(
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: deviceSize.width*0.07,
              vertical: deviceSize.height*0.04,
            ),
            width: deviceSize.width,
            height: deviceSize.height,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Image.asset("images/n.png",height: 50,),
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 20,bottom: 30),
                  ),
                  Text(
                    'Login',
                    style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 49,),
                  ),
                  const SizedBox(height: 20),
                  Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          height: deviceSize.height*0.1,
                          child:
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).primaryColor),
                            decoration: InputDecoration(
                              hintText: 'Email',
                              hintStyle: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey)
                            ),
                            controller: emailController,
                            validator: (value) {
                              return emailErrorMessage(value);
                            },
                          ),
                        ),
                        Container(
                              alignment: Alignment.centerLeft,
                              height: deviceSize.height*0.1,
                              child:
                              TextFormField(
                                keyboardType: TextInputType.text,
                                style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).primaryColor),
                                decoration: InputDecoration(
                                  // icon: Icon(Icons.vpn_key),
                                  hintText: 'Password',
                                  suffixIcon: IconButton(
                                    onPressed: () => _togglePasswordVisibility(),
                                    icon: _isPasswordVisible ? const Icon(Icons.visibility_off_outlined) : const Icon(Icons.visibility_outlined),
                                  ),
                                ),
                                controller: passwordController,
                                validator: (value) {
                                  return passwordErrorMessage(value);
                                },
                                obscureText: !_isPasswordVisible,
                              ),
                            ),
                        // GestureDetector(
                        //   onTap: () {},
                        //   child: Container(
                        //     margin: EdgeInsets.only(
                        //       top: deviceSize.height*0.05,
                        //       bottom: 6,
                        //     ),
                        //     alignment: Alignment.center,
                        //     child: RichText(
                        //       text: TextSpan(
                        //         text: 'Forgot password?',
                        //         style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey),
                        //       )
                        //     ),
                        //   ),
                        // ),
                        SizedBox( // comment this box out once Forgot Password is wired up
                          height: deviceSize.height*0.05+6,
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            bottom: deviceSize.height*0.02,
                          ),
                          padding: EdgeInsets.only(bottom: deviceSize.height*0.02),
                          width: deviceSize.width,
                          // height: deviceSize.height*0.1,
                          alignment: Alignment.center,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              // elevation: MaterialStateProperty.all<double>(5.0),
                              // padding: MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.all(15.0)),
                              shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              )),
                              backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).colorScheme.primary),
                              alignment: Alignment.center,
                              fixedSize: MaterialStateProperty.all<Size>(Size(deviceSize.width,50))
                            ),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                _bloc.signInWithEmail(emailController.text.trim(), passwordController.text.trim());
                              }
                            },
                            child: Text(
                              'Login',
                              style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white,fontSize: 16),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // _buildSignInWithText(),
                  // const SizedBox(
                  //   height: 25,
                  // ),
                  _buildSignInWithText(),
                  _buildSocialBtnRow(deviceSize),
                  _buildSignupInsteadButton(deviceSize),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSignupInsteadButton(Size deviceSize) {
    return Center(
      child: GestureDetector(
        onTap: () => Navigator.of(context).pushNamed(RoutesDirectory.register),
        child: RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "Don't have an account? ",
                style: Theme.of(context).textTheme.bodyText2
              ),
              TextSpan(
                  text: "Sign Up",
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).colorScheme.primary,fontWeight: FontWeight.w600)
              ),
            ]
          ),
        ),
      ),
    );
  }

  Widget _buildSignInWithText() {
    return Container(
      alignment: Alignment.center,
      child: RichText(
        text: TextSpan(
          text: 'OR LOGIN WITH',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.black,fontWeight: FontWeight.w600),
        ),
      ),
    );
  }

  Widget _buildSocialBtn(Function()? onTap, AssetImage logo){
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          image: DecorationImage(
            image: logo,
          ),
        ),
      ),
    );
  }
  Widget _buildSocialBtnRow(Size deviceSize) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: deviceSize.height*0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildSocialBtn(
                () {
              AppUtility.googleLogin(context);
            },
            const AssetImage(
              'images/googleLogo.png',
            ),
          ),
          _buildSocialBtn(
                () {
              AppUtility.facebookLogin(context);
            },
            const AssetImage(
              'images/facebookLogo.png',
            ),
          ),
          if(Platform.isIOS)...[
            _buildSocialBtn(
                  () {
                AppUtility.loginWithApple(context);
              },
              const AssetImage(
                'images/apple_logo.png',
              ),
            ),
          ],
        ],
      ),
    );
  }
}