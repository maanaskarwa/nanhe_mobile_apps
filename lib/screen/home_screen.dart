import 'package:carousel_slider/carousel_slider.dart';
import 'package:clipboard/clipboard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_social_content_share/flutter_social_content_share.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nanhe/bloc/api_repository.dart';
import 'package:nanhe/model/child_data_from_parent.dart';
import 'package:nanhe/model/user_data.dart';
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/util/data_repository.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late UserData userData;
    APIRepository().getUserData(FirebaseAuth.instance.currentUser!.uid).then((value) => userData = value!);
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(
            left: deviceSize.width * 0.07,
            right: deviceSize.width * 0.07,
            top: deviceSize.height * 0.03,
            bottom: deviceSize.height*0.01
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                //Graphic with stats
                Stack(children: [
                  Container(
                    decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        colors: [
                          Color(0xffFF8CCF),
                          Color(0xff77B2FF),
                        ],
                      ),
                      borderRadius: BorderRadius.circular(10)),
                    width: deviceSize.width,
                    height: deviceSize.height * 0.2,
                    child: const Text(''),
                  ),
                  Positioned(
                    child: SvgPicture.asset(
                      'images/homeScreenGraphicOne.svg',
                    ),
                    right: 0,
                    top: deviceSize.height * 0.025,
                  ),
                  Positioned(
                    left: 10,
                    top: 10,
                    width: deviceSize.width*0.5,
                    height: deviceSize.height * 0.2,
                    child: CarouselSlider(
                      options: CarouselOptions(
                        autoPlay: true,
                        autoPlayInterval: const Duration(seconds: 4),
                        viewportFraction: 1,
                        enableInfiniteScroll: true,
                        pauseAutoPlayOnManualNavigate: false,
                      ),
                      items: [
                        RichText(
                          text: TextSpan(
                              text: '''40,000 Children
are kidnapped
each year
in India!''',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(color: Colors.white)),
                        ),
                        RichText(
                          text: TextSpan(
                              text: '''Majority of
them are forced
into begging
and labor!''',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(color: Colors.white)),
                        ),
                        RichText(
                          text: TextSpan(
                              text: '''Over 300,000
children are
forced to beg
in India!''',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(color: Colors.white)),
                        ),
                      ],
                    ),
                  ),
                  ],
                ),
                // nanhe stats
                Card(
                  margin: const EdgeInsets.only(top: 15),
                  color: const Color(0xFFF3FCFF),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    child: StreamBuilder<DocumentSnapshot?>(
                      stream: APIRepository().getSummaryStats,
                      builder: (context, snapshot) {
                        if(snapshot.hasData) {
                          final data = snapshot.data!.data() as Map<String,dynamic>;
                          final volunteerCount = data['volunteerCount'].toString();
                          final parentCount = data['parentCount'].toString();
                          final volunteerPictureCount = data['volunteerPictureCount'].toString();
                          final parentPictureCount = data['parentPictureCount'].toString();
                          return StatsColumn(
                            deviceSize: deviceSize,
                            volunteerCount: volunteerCount,
                            parentCount: parentCount,
                            volunteerPictureCount: volunteerPictureCount,
                            parentPictureCount: parentPictureCount
                          );
                        }
                        return StatsColumn(
                            deviceSize: deviceSize,
                            volunteerCount: '...',
                            parentCount: '...',
                            volunteerPictureCount: '...',
                            parentPictureCount: '...'
                        );
                      }
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 15,bottom: 10),
                  alignment: Alignment.centerLeft,
                  child: RichText(
                    text: TextSpan(
                      text: '''Crushed Childhoods... 😥''',
                      style: Theme.of(context).textTheme.headline6!.copyWith(fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
               Row(
                 children: [
                   Expanded(
                     child: Material(
                       elevation: 5,
                       child: Container(
                         margin: const EdgeInsets.symmetric(horizontal: 5),
                         child: YoutubePlayerIFrame(
                           controller: YoutubePlayerController(
                             //dynamic url
                             initialVideoId: YouTubeVideoIdRepository.getVideoId,
                             params: const YoutubePlayerParams(
                               autoPlay: false,
                               showFullscreenButton: true
                             )
                           ),
                         ),
                       ),
                     ),
                   ),
                 ],
               ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child: Row(
            children: [
              GestureDetector(
              child: Container(
                height: 30,
                width: 30,
                alignment: Alignment.center,
                padding: const EdgeInsets.only(bottom: 2.5),
                decoration:  BoxDecoration(
                  borderRadius: BorderRadius.circular(7.5),
                  gradient: const LinearGradient(
                    colors: [
                    Color(0xffF58529),
                    Color(0xffFEDA77),
                    Color(0xffDD2A7B),
                    Color(0xff8134AF),
                    Color(0xff515BD4),
                    ],
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight
                  ),
                ),
                child: const Icon(
                FontAwesomeIcons.instagram,
                  color: Colors.white,
                  size: 27,
                ),
              ),
              onTap: () async {
                FlutterClipboard.copy('''Please check nanhe.org to help missing children reunite with their families.''');
                AppUtility.showMessageDialog(
                  context,
                  'To save your time, we have created the caption for your post. Please paste it in Instagram.',
                  onPressed: () async {
                    Navigator.of(context).pop();
                    final response = await FlutterSocialContentShare.share(
                      type: ShareType.instagramWithImageUrl,
                      imageUrl: 'https://firebasestorage.googleapis.com/v0/b/nanhe-20ea0.appspot.com/o/BG%20Img%201.jpg?alt=media&token=d35bfd18-f480-4ade-a285-c51c0e0d79c3',
                    );
                  }
                );
              },
            ),
              const Spacer(),
              GestureDetector(
                child: Icon(
                FontAwesomeIcons.whatsapp,
                color: Color.lerp(const Color.fromRGBO(37, 211, 102, 1), const Color.fromRGBO(18, 140, 126, 1), 0),
                size: 30,
                ),
                onTap: () async {
                  final response = await FlutterSocialContentShare.shareOnWhatsapp('0', '''Hello,
${userData.name} here. Did you know that over 40,000 children are kidnapped every year in India and then forced into begging/child labor? 😔
            
The good news is that there's an AI-driven app to help them reunite with their parents. Yes, *nanhe* app, made by a teenager, lets you do that. Parents can upload pics of their missing children, people like you and me can upload pictures of children begging at traffic signals etc, and nanhe.org does the matching!!
            
Download the app and help rescue children. CHILDREN ARE OUR FUTURE!!
            
Android: https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app
            
_Please forward this message ONLY AFTER you upload at least one child's picture._''',
                  );
                },
              ),
              const Spacer(),
              GestureDetector(
                child: const Icon(
                  FontAwesomeIcons.facebook,
                  color: Color.fromRGBO(59, 89, 152, 1),
                  size: 30,
                ),
                onTap: () async {
                  final response = await FlutterSocialContentShare.share(
                  type: ShareType.facebookWithoutImage,
                  quote: 'Please check nanhe.org to help missing children reunite with their families.',
                  url: 'https://www.nanhe.org',
                  );
                },
              ),
              const Spacer(flex: 2,),
              RichText(text: TextSpan(text: "Spread the word!",style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 16))),
            // const Spacer(),
            ].reversed.toList(),
          ),
        ),
      )
    );
  }

  List<ChildDataFromParent> sortByDate(List<ChildDataFromParent> childList) {
    List<ChildDataFromParent> newList = childList;
    newList.sort((a,b) => b.missingSince.compareTo(a.missingSince));
    return newList;
  }
}

// used to display nanhe stats
class StatsColumn extends StatelessWidget {
  const StatsColumn({
    Key? key,
    required this.deviceSize,
    required this.volunteerCount,
    required this.parentCount,
    required this.volunteerPictureCount,
    required this.parentPictureCount,
  }) : super(key: key);

  final Size deviceSize;
  final String volunteerCount;
  final String parentCount;
  final String volunteerPictureCount;
  final String parentPictureCount;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 7),
          child: RichText(
            text: TextSpan(
                text: 'nanhe stats',
                style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 18)
            ),
          ),
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 6),
              width: deviceSize.width*0.4,
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: '$volunteerCount  ',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w700,fontSize: 14),
                    ),
                    TextSpan(
                      text: 'Volunteers',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 14)
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 6),
              width: deviceSize.width*0.4,
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: '$parentCount  ',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w700,fontSize: 14),
                    ),
                    TextSpan(
                        text: 'Parents',
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 14)
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 18),
              width: deviceSize.width*0.4,
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: '$volunteerPictureCount  ',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w700,fontSize: 14),
                    ),
                    TextSpan(
                        text: 'Pics uploaded',
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 14)
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 18),
              width: deviceSize.width*0.4,
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: '$parentPictureCount  ',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w700,fontSize: 14),
                    ),
                    TextSpan(
                      text: 'Missing children',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 14),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
