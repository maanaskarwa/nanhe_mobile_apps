import 'package:clipboard/clipboard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_social_content_share/flutter_social_content_share.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_repository.dart';
import 'package:nanhe/bloc/api_state.dart';
import 'package:nanhe/model/child_data_from_parent.dart';
import 'package:nanhe/model/json_serializable_utils.dart';
import 'package:nanhe/route_generator.dart' show RoutesDirectory;
import 'package:nanhe/util/app_constants.dart';
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/widgets/app_drawer.dart';
import 'package:nanhe/widgets/image_view.dart';
import 'package:nanhe/widgets/no_data_view.dart';

class ParentDashboard extends StatefulWidget {
  const ParentDashboard({Key? key}) : super(key: key);
  @override
  _ParentDashboardState createState() => _ParentDashboardState();
}

class _ParentDashboardState extends State<ParentDashboard> with AutomaticKeepAliveClientMixin {
  late final APIBloc bloc;

  @override
  void initState() {
    bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final deviceSize = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        drawer: AppDrawer(CurrentScreen.none),
        body: BlocListener(
          bloc: bloc,
          listener: (context,state) {
            if(state is APILoadingState) {
              AppUtility.showProgressDialog(context);
            }
            if(state is APILoadedState) {
              Navigator.pop(context);
              setState(() {});
              //nothing to display here
            }
            if(state is APIErrorState) {
              Navigator.pop(context);
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
            child: Column(
              children: [
                Expanded(
                  child: StreamBuilder(
                    stream: APIRepository().getChildrenUploadedByParentByUserId(FirebaseAuth.instance.currentUser!.uid),
                    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                        List<ChildDataFromParent> childrenList = snapshot.data!.docs
                          .map((e) => ChildDataFromParent.fromJson(e.data() as Map<String,dynamic>)).toList();
                        childrenList.sort((a,b) => b.missingSince.compareTo(a.missingSince));
                        return ListView.builder(
                          physics: const ClampingScrollPhysics(),
                          itemBuilder: (context,i) {
                            if(i == childrenList.length) {
                              return Container(
                                margin: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
                                height: 200,
                                child: Material(
                                  borderRadius: BorderRadius.circular(10),
                                  elevation: 3,
                                  child: Container(
                                    padding: const EdgeInsets.all(3),
                                    child: DottedBorder(
                                      borderType: BorderType.RRect,
                                      dashPattern: const [5],
                                      radius: const Radius.circular(10),
                                      strokeWidth: 2,
                                      color: Colors.grey,
                                      child: TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pushNamed(RoutesDirectory.addChildDataAsParent);
                                        },
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            const Icon(Icons.add,size: 50,color: Colors.grey,),
                                            Center(
                                              child: RichText(
                                                text: TextSpan(
                                                    text: 'Upload',
                                                    style: Theme.of(context).textTheme.button!.copyWith(fontSize: 18,color: Colors.grey,fontWeight: FontWeight.w600)
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }
                            final childData = childrenList[i];
                            return SingleChildScrollView(
                              child: Container(
                                // height: 400,
                                margin: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
                                child: Material(
                                  elevation: 3,
                                  borderRadius: BorderRadius.circular(8),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 7,top: 7,right: 7,bottom: 10),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Row(
                                          children: childData.faceImages!.map((e) => LimitedBox(
                                            maxWidth: deviceSize.width*0.3 ,child: ImageView(e,ImageType.url,width: deviceSize.width*0.3,fit: BoxFit.scaleDown,)),).toList(),
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: [
                                            RichText(text: TextSpan(text: childData.name,style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 16,fontWeight: FontWeight.w600))),
                                            if(!childData.found)buildEditButton(childData),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            RichText(
                                              text: TextSpan(
                                                children: [
                                                  TextSpan(
                                                    text: ' ${childData.gender.toString().substring(childData.gender.toString().indexOf('.')+1)}',
                                                    style: Theme.of(context).textTheme.bodyText2,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            RichText(
                                              text: TextSpan(
                                                children: [
                                                  TextSpan(
                                                    text: '${AppUtility.getAge(childData.dob)}',
                                                    style: Theme.of(context).textTheme.bodyText2,
                                                  ),
                                                  TextSpan(
                                                      text: ' years',
                                                      style: Theme.of(context).textTheme.bodyText2//.copyWith(fontSize: 10,color: Color(0xff9D9D9D)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        if(!childData.found && childData.isActive && !childData.matchFound) RichText(
                                          text: TextSpan(
                                            children: [
                                              TextSpan(
                                                text: 'Missing:',
                                                style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 12,)//color: Color(0xff9D9D9D)),
                                              ),
                                              TextSpan(
                                                text: ' ${DateFormat(Constants.displayDateFormat).format(childData.missingSince)}',
                                                style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600,fontSize: 12),
                                              ),
                                            ],
                                          ),
                                        )
                                        else if(!childData.found && childData.isActive && childData.matchFound) RichText(text: TextSpan(text: 'MATCH FOUND',style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600,fontSize: 16,color: Theme.of(context).colorScheme.primary)))
                                        else if(!childData.found && !childData.isActive) RichText(text: TextSpan(text: 'INACTIVE',style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600,fontSize: 16,color: Colors.grey)))
                                        else if(childData.found) RichText(text: TextSpan(text: 'FOUND',style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600,fontSize: 16,color: Colors.green))),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            ..._buildSocialShareButtons(childData),
                                          ],
                                        ),
                                        // SocialShare(childData: childData),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: childrenList.length + 1,
                        );
                      } else {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            NoDataView(
                              context,
                              noDataText: '''You have not reported any missing children yet.''',
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 25),
                              child: TextButton(
                                onPressed: () {
                                  Navigator.of(context).pushNamed(RoutesDirectory.addChildDataAsParent);
                                },
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Report Now',
                                      style: Theme.of(context).textTheme.headline6!.copyWith(color: Theme.of(context).colorScheme.primary)
                                  ),
                                ),
                                style: const ButtonStyle(
                                  // side: MaterialStateProperty.all<BorderSide>(BorderSide(color: Theme.of(context).colorScheme.primary))
                                ),
                              ),
                            )
                          ],
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget buildMarkAsFoundButton(ChildDataFromParent childData) {
    return IconButton(
      tooltip: 'Mark as found',
      onPressed: () {
        var child = childData;
        child.found = true;
        child.updatedAt = DateTime.now();
        bloc.updateChildDataAsParent(child.uid, child);
      },
      icon: Icon(Icons.check,color: Theme.of(context).colorScheme.primary,),
    );
  }

  Widget buildShareButton(num buttonWidth,ChildDataFromParent childData) {
    return IconButton(
      onPressed: () async {
        await FlutterSocialContentShare.shareOnWhatsapp(
          '0',
          '''Hello,
My child ${childData.name} is missing. Please help me find ${childData.gender == Gender.male?'him' : childData.gender == Gender.female?'her' : 'them'}. I have heard about gangs which kidnap children, move them to another town, and force them into begging or labor 😨

Please use the nanhe app and upload pictures of any child you see begging 🙏

God bless you!

Android: https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app''',
        );
      },
      icon: Icon(
        FontAwesomeIcons.whatsapp,
        color: Color.lerp(const Color.fromRGBO(37, 211, 102, 1), const Color.fromRGBO(18, 140, 126, 1), 0),
        size: 30,
      ),
    );
  }

  Widget buildEditButton(ChildDataFromParent childData) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5),
      child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed(
                RoutesDirectory.addChildDataAsParent,
              arguments: {
                'previousChildData': childData,
              }
            );
          },
          child: Icon(Icons.edit,color: Theme.of(context).colorScheme.primary,size: 20,),
        ),
    );
  }

  Widget buildMarkAsInactiveButton(ChildDataFromParent childData) {
    return IconButton(
      onPressed: () {
        var child = childData;
        child.isActive = false;
        child.updatedAt = DateTime.now();
        bloc.updateChildDataAsParent(child.uid, child);
      },
      tooltip: 'Mark as Inactive',
      icon: Icon(Icons.block,color: Theme.of(context).colorScheme.primary,),
    );
  }

  List<Widget> _buildSocialShareButtons(ChildDataFromParent childData) {
    return [
        IconButton(
          onPressed: () async {
            await FlutterSocialContentShare.share(
                type: ShareType.facebookWithoutImage,
                quote: 'My child ${childData.name} is missing since ${DateFormat(Constants.displayDateFormat).format(childData.missingSince)}. Please download the nanhe app from nanhe.org to help me find ${childData.gender == Gender.male? 'him' : childData.gender == Gender.female? 'her' : 'them'}. Here is ${childData.gender == Gender.male? 'his' : childData.gender == Gender.female? 'her' : 'their'} picture.',
                url: childData.images!.first
            );
          },
          icon: const Icon(
            FontAwesomeIcons.facebook,
            color: Color.fromRGBO(59, 89, 152, 1),
            size: 25,
          ),
        ),
        IconButton(
          onPressed: () async {
            await FlutterSocialContentShare.shareOnWhatsapp(
              '0',
              '''My child ${childData.name} is missing since ${DateFormat(Constants.displayDateFormat).format(childData.missingSince)}. Please help me find ${childData.gender == Gender.male?'him' : childData.gender == Gender.female?'her' : 'them'}. I have heard about gangs which kidnap children, move them to another town, and force them into begging or labor 😨

Please use the nanhe app and upload pictures of any child you see begging 🙏

God bless you!

Android: https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app''',
            );
          },
          icon: Icon(
            FontAwesomeIcons.whatsapp,
            color: Color.lerp(const Color.fromRGBO(37, 211, 102, 1), const Color.fromRGBO(18, 140, 126, 1), 0),
            size: 25,
          ),
        ),
      GestureDetector(
        child: Container(
          height: 25,
          width: 25,
          margin: const EdgeInsets.all(5),
          alignment: Alignment.center,
          padding: const EdgeInsets.only(left:1,bottom: 1.5),
          decoration:  BoxDecoration(
            borderRadius: BorderRadius.circular(7.5),
            gradient: const LinearGradient(
                colors: [
                  Color(0xffF58529),
                  Color(0xffFEDA77),
                  Color(0xffDD2A7B),
                  Color(0xff8134AF),
                  Color(0xff515BD4),
                ],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight
            ),
          ),
          child: const Icon(
            FontAwesomeIcons.instagram,
            color: Colors.white,
            size: 22,
          ),
        ),
        onTap: () async {
          FlutterClipboard.copy('My child ${childData.name} is missing since ${DateFormat(Constants.displayDateFormat).format(childData.missingSince)}. Please download the nanhe app from nanhe.org to help me find ${childData.gender == Gender.male? 'him' : childData.gender == Gender.female? 'her' : 'them'}. Here is ${childData.gender == Gender.male? 'his' : childData.gender == Gender.female? 'her' : 'their'} picture.');
          AppUtility.showMessageDialog(
              context,
              '''To save your time, we have created the caption for your post. Please paste it in Instagram.
              
It may take a few seconds...''',
              onPressed: () async {
                final response = await FlutterSocialContentShare.share(
                  type: ShareType.instagramWithImageUrl,
                  imageUrl: childData.images!.first,
                );
                Navigator.of(context).pop();
              }
          );
        },
      ),
    ];
  }

  Widget buildShowMatchesButton() {
    return TextButton.icon(
      onPressed: () {
        //TODO: show matches found
      },
      label: Text(
        'Show Matches',
        style: Theme.of(context).textTheme.bodyText2!.copyWith(
            color: Theme.of(context).primaryColor,
            fontSize: 13
        ),
      ),
      icon: const Icon(Icons.photo),
      style: ButtonStyle(
        shape: MaterialStateProperty.all<OutlinedBorder>(
          RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
            borderRadius: BorderRadius.circular(3),
          ),
        ),
      ),
    );
  }

  Widget buildResumeMatchingButton( ChildDataFromParent childData) {
    return TextButton.icon(
      onPressed: () {
        var child = childData;
        child.isActive = true;
        child.updatedAt = DateTime.now();
        bloc.updateChildDataAsParent(child.uid, child);
      },
      label: Text(
        'Resume Matching',
        style: Theme.of(context).textTheme.bodyText2!.copyWith(
            color: Theme.of(context).primaryColor,
            fontSize: 13
        ),
      ),
      icon: const Icon(Icons.restart_alt),
      style: ButtonStyle(
        shape: MaterialStateProperty.all<OutlinedBorder>(
          RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
            borderRadius: BorderRadius.circular(3),
          ),
        ),
      ),
    );
  }
}

class SocialShare extends StatelessWidget {
  const SocialShare({
    Key? key,
    required this.childData,
  }) : super(key: key);

  final ChildDataFromParent childData;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        IconButton(
          onPressed: () async {
            await FlutterSocialContentShare.share(
              type: ShareType.facebookWithoutImage,
              quote: 'My child is missing. Please use the nanhe app to help me find ${childData.gender == Gender.male? 'him' : childData.gender == Gender.female? 'her' : 'them'}.',
              url: 'https://www.nanhe.org',
            );
          },
          icon: const Icon(
            FontAwesomeIcons.facebookSquare,
            color: Color.fromRGBO(59, 89, 152, 1),
            size: 30,
          ),
        ),
        IconButton(
          onPressed: () async {
            await FlutterSocialContentShare.shareOnWhatsapp(
              '0',
              '''Hello,
My child ${childData.name} is missing. Please help me find ${childData.gender == Gender.male?'him' : childData.gender == Gender.female?'her' : 'them'}. I have heard about gangs which kidnap children, move them to another town, and force them into begging or labor 😨

Please use the nanhe app and upload pictures of any child you see begging 🙏

God bless you!

Android: https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app''',
            );
          },
          icon: Icon(
            FontAwesomeIcons.whatsapp,
            color: Color.lerp(const Color.fromRGBO(37, 211, 102, 1), const Color.fromRGBO(18, 140, 126, 1), 0),
            size: 30,
          ),
        ),
      ],
    );
  }
}
