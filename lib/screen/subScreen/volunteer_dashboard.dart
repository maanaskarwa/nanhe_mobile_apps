import 'package:clipboard/clipboard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_social_content_share/flutter_social_content_share.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_repository.dart';
import 'package:nanhe/model/child_data_from_volunteer.dart';
import 'package:nanhe/route_generator.dart' show RoutesDirectory;
import 'package:nanhe/util/app_constants.dart';
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/widgets/image_view.dart';
import 'package:nanhe/widgets/no_data_view.dart';

class VolunteerDashboard extends StatefulWidget {
  const VolunteerDashboard({Key? key}) : super(key: key);

  @override
  _VolunteerDashboardState createState() => _VolunteerDashboardState();
}

class _VolunteerDashboardState extends State<VolunteerDashboard> with AutomaticKeepAliveClientMixin {
  late final APIBloc bloc;

  @override
  void initState() {
    bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: StreamBuilder(
                stream: APIRepository().getChildrenUploadedByVolunteerByUserId(FirebaseAuth.instance.currentUser!.uid),
                builder: (context, AsyncSnapshot<QuerySnapshot>? snapshot) {
                  if (snapshot != null) {
                    if (snapshot.hasData) {
                      List<ChildDataFromVolunteer> childList = snapshot.data!.docs.map((e) => ChildDataFromVolunteer.fromJson(e.data() as Map<String,dynamic>)).toList();
                      childList.sort((a,b) => b.createdAt.compareTo(a.createdAt));
                      if (childList.isNotEmpty) {
                        return GridView.builder(
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
                            childAspectRatio: deviceSize.aspectRatio*1.125,
                          ),
                          physics: const ClampingScrollPhysics(),
                          itemBuilder: (context,i) {
                            if(i == childList.length) {
                              return Container(
                                margin: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
                                child: Material(
                                  borderRadius: BorderRadius.circular(10),
                                  elevation: 3,
                                  child: Container(
                                    padding: const EdgeInsets.all(3),
                                    child: DottedBorder(
                                      borderType: BorderType.RRect,
                                      dashPattern: const [5],
                                      radius: const Radius.circular(10),
                                      strokeWidth: 2,
                                      color: Colors.grey,
                                      child: TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pushNamed(RoutesDirectory.volunteerCamera);
                                        },
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            const Icon(Icons.add,size: 50,color: Colors.grey,),
                                            Center(
                                              child: RichText(
                                                text: TextSpan(
                                                  text: 'Upload',
                                                  style: Theme.of(context).textTheme.button!.copyWith(fontSize: 18,color: Colors.grey,fontWeight: FontWeight.w600)
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }
                            final childData = childList[i];
                            return SingleChildScrollView(
                              child: Container(
                                // height: 400,
                                margin: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
                                child: Material(
                                  elevation: 3,
                                  borderRadius: BorderRadius.circular(8),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 7,top: 7,right: 7,bottom: 10),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: ImageView(childData.images!.first,ImageType.url,height: 180,),
                                          alignment: Alignment.center,
                                          // margin: EdgeInsets.only(bottom: 8),
                                        ),
                                        RichText(
                                          text: TextSpan(
                                            text: DateFormat(Constants.displayDateTimeFormat).format(childData.createdAt),
                                            style: Theme.of(context).textTheme.bodyText2
                                          )
                                        ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: [
                                              IconButton(
                                                onPressed: () async {
                                                  await FlutterSocialContentShare.share(
                                                    type: ShareType.facebookWithoutImage,
                                                    quote: "Proud to be a nanhe.org volunteer to help rescue children from begging. Here is the child I found.",
                                                    url: childData.images!.first,
                                                  );
                                                },
                                                icon: const Icon(
                                                  FontAwesomeIcons.facebook,
                                                  color: Color.fromRGBO(59, 89, 152, 1),
                                                  size: 25,
                                                ),
                                              ),
                                              IconButton(
                                                onPressed: () async {
                                                  await FlutterSocialContentShare.shareOnWhatsapp(
                                                    '0',
                                                    '''Hello,
I just helped rescue a child from begging 😊
        
You can do it too. It's simple. Just download the *nanhe* app and use it to upload pictures of children who are forced to beg at traffic signals. nanhe.org matches these pictures with pictures uploaded by parents of missing children, and helps the children reunite with their parents.
        
Be a *nanhe volunteer!!* 😎
        
Android: https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app
        
_Please forward this msg ONLY AFTER you upload at least one child's picture_''',
                                                  );
                                                },
                                                icon: Icon(
                                                  FontAwesomeIcons.whatsapp,
                                                  color: Color.lerp(const Color.fromRGBO(37, 211, 102, 1), const Color.fromRGBO(18, 140, 126, 1), 0),
                                                  size: 25,
                                                ),
                                              ),
                                              InkWell(
                                                child: Container(
                                                  margin: const EdgeInsets.all(5),
                                                  height: 25,
                                                  width: 25,
                                                  alignment: Alignment.center,
                                                  padding: const EdgeInsets.only(left:1,bottom: 1.5),
                                                  decoration:  BoxDecoration(
                                                    borderRadius: BorderRadius.circular(7.5),
                                                    gradient: const LinearGradient(
                                                      colors: [
                                                        Color(0xffF58529),
                                                        Color(0xffFEDA77),
                                                        Color(0xffDD2A7B),
                                                        Color(0xff8134AF),
                                                        Color(0xff515BD4),
                                                      ],
                                                      begin: Alignment.bottomLeft,
                                                      end: Alignment.topRight,
                                                    ),
                                                  ),
                                                  child: const Icon(
                                                    FontAwesomeIcons.instagram,
                                                    color: Colors.white,
                                                    size: 22,
                                                  ),
                                                ),
                                                onTap: () async {
                                                  FlutterClipboard.copy('''Proud to be a nanhe.org volunteer and help rescue children from begging. Here is the child I found.''');
                                                  AppUtility.showMessageDialog(
                                                      context,
                                                      'Sharing the picture. Please paste the text in Instagram.',
                                                      onPressed: () async {
                                                        Navigator.of(context).pop();
                                                        final response = await FlutterSocialContentShare.share(
                                                          type: ShareType.instagramWithImageUrl,
                                                          imageUrl: childData.images!.first,
                                                        );
                                                      }
                                                  );
                                                },
                                              )
                                            ],
                                          ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: childList.length+1,
                        );
                      } else {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            NoDataView(
                              context,
                              noDataText: '''You have not uploaded any pictures yet.''',
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 25),
                              child: TextButton(
                                onPressed: () {
                                  Navigator.of(context).pushNamed(RoutesDirectory.volunteerCamera);
                                },
                                child: RichText(
                                  text: TextSpan(
                                    text: 'Upload Now',
                                    style: Theme.of(context).textTheme.headline6!.copyWith(color: Theme.of(context).colorScheme.primary)
                                  ),
                                ),
                                style: const ButtonStyle(
                                  // side: MaterialStateProperty.all<BorderSide>(BorderSide(color: Theme.of(context).colorScheme.primary))
                                ),
                              ),
                            )
                          ],
                        );
                      }
                    } else {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          NoDataView(
                            context,
                            noDataText: '''You have not uploaded any pictures yet.''',
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 25),
                            child: TextButton(
                              onPressed: () {
                                Navigator.of(context).pushNamed(RoutesDirectory.volunteerCamera);
                              },
                              child: RichText(
                                text: TextSpan(
                                    text: 'Upload Now',
                                    style: Theme.of(context).textTheme.headline6!.copyWith(color: Theme.of(context).colorScheme.primary)
                                ),
                              ),
                              style: const ButtonStyle(
                                  // side: MaterialStateProperty.all<BorderSide>(BorderSide(color: Theme.of(context).colorScheme.primary))
                              ),
                            ),
                          )
                        ],
                      );
                    }
                  } else {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        NoDataView(
                          context,
                          noDataText: '''You have not uploaded any pictures yet.''',
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 25),
                          child: TextButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed(RoutesDirectory.volunteerCamera);
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: const Text("""Please focus on the child's face. 
Avoid other faces."""),
                                  duration: const Duration(seconds: 15),
                                  action: SnackBarAction(label: 'Okay', onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar()),
                                ),
                              );
                            },
                            child: RichText(
                              text: TextSpan(
                                  text: 'Upload Now',
                                  style: Theme.of(context).textTheme.headline6!.copyWith(color: Theme.of(context).colorScheme.primary)
                              ),
                            ),
                            style: const ButtonStyle(
                              // side: MaterialStateProperty.all<BorderSide>(BorderSide(color: Theme.of(context).colorScheme.primary,)),
                            ),
                          ),
                        )
                      ],
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
