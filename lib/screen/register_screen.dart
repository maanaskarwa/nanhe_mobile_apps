import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/location.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_state.dart';

import '../model/user_data.dart';
import '../route_generator.dart' show RoutesDirectory;
import '../util/app_utility.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late final APIBloc _bloc;
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool _isPasswordVisible = true;
  bool _isConfirmPasswordVisible = false;

  final _formKey = GlobalKey<FormState>();
  late Location? location;
  late LocationData? locationData;

  String? emailErrorMessage (String? email) {
    if (email == null) {
      return 'Please enter an email address.';
    } else if (email.isEmpty) {
      return 'Please enter an email address.';
    } else if (!email.contains('@')) {
      return 'Please enter a valid email address.';
    } else if (!email.contains('.')) {
      return 'Please enter a valid email address.';
    } else {
      return null;
    }
  }

  String? passwordErrorMessage (String? password) {
    if (password == null) {
      return 'Please enter a password.';
    } else if (password.isEmpty) {
      return 'Please enter a password.';
    } else if (password.length < 6) {
      return 'Please enter a password of at least 6 characters.';
    } else {
      return null;
    }
  }

  String? nameErrorMessage (String? name) {
    if (name == null) {
      return 'Please enter your name.';
    } else if (name.isEmpty) {
      return 'Please enter your name.';
    } else {
      return null;
    }
  }

  void _togglePasswordVisibility(bool trueIfPwdAndFalseIfConfirmPwd) {
    setState(() {
      if(trueIfPwdAndFalseIfConfirmPwd) {
        _isPasswordVisible = !_isPasswordVisible;
      } else {
        _isConfirmPasswordVisible = !_isConfirmPasswordVisible;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
  }
  @override
  void initState() {
    _bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
    location = Location.instance;
    Future.delayed(Duration.zero, () async {
      bool serviceOn = await AppUtility.enableLocationService();
      if (serviceOn) {
        location!.onLocationChanged.listen((event) {
          if (mounted) {
            locationData = event;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return BlocListener(
        bloc: _bloc,
        listener: (BuildContext context, state) async {
          if (state is APIErrorState) {
            Navigator.pop(context);
            Navigator.pop(context);
            String errorMessage;
            if(state.error.contains("[firebase_auth/email-already-in-use]")) {
              errorMessage = '''This email is already registered.
Please login using the same authentication as before.''';
            } else if (state.error.contains("[firebase_auth/invalid-email]")) {
              errorMessage = '''Invalid email.
Please use a valid email.''';
            } else if (state.error.contains("[firebase_auth/weak-password]")) {
              errorMessage = '''Weak password.
Please enter a stronger password.''';
            } else {
              errorMessage = '''An error occurred.
Please try again later.''';
            }
            AppUtility.showMessageDialog(
              context,
              errorMessage,
              onPressed: () {
                Navigator.of(context).pop();
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
                if(!FocusScope.of(context).hasPrimaryFocus) {
                  FocusScope.of(context).unfocus();
                }
              },
            );
          }
          if (state is APILoadingState) {
            AppUtility.showProgressDialog(context);
          }
          if (state is APILoadedState<UserCredential>) {
            Navigator.pop(context);
            if(state.data.user!.emailVerified) {
              if(state.data.additionalUserInfo!.isNewUser) {
                AppUtility.showMessageDialog(
                    context,
                    '''Welcome to the nanhe journey of

SAVING CHILDREN 👍

Next time you see a child in begging/labor, just start the app and we'll take you directly to the camera screen so you can upload a picture quickly!!''',
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.mainDashboard, (route) => false,);
                    }
                );
              } else {
                Navigator.of(context).pushReplacementNamed(RoutesDirectory.volunteerCamera,);
              }
            } else {
              AppUtility.showMessageDialog(
                context,
                'A verification email has been sent to your email address. Please verify, come back, and log in.',
                onPressed: () {
                  Navigator.of(context).pop();
                  FocusScope.of(context).unfocus();
                  Navigator.of(context).pop();
                }
              );
            }
          }
        },
        child: Scaffold(
          body: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
              child: Container(
                padding: EdgeInsets.only(
                  left: deviceSize.width*0.07,
                  right: deviceSize.width*0.07,
                  top: deviceSize.height*0.04,
                ),
                width: deviceSize.width,
                height: deviceSize.height,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Sign Up',
                        style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 49,),
                      ),
                      SizedBox(height: deviceSize.height*0.02),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              height: deviceSize.height*0.1,
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).primaryColor),
                                decoration: const InputDecoration(
                                    hintText: 'Your name'
                                ),
                                validator: (value) {
                                  return nameErrorMessage(value);
                                },
                                controller: nameController,
                              )
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              height: deviceSize.height*0.1,
                              child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).primaryColor),
                                decoration: const InputDecoration(
                                    hintText: 'Email'
                                ),
                                validator: (value) {
                                  return emailErrorMessage(value);
                                },
                                controller: emailController,
                              )
                            ),
                            Container(
                                alignment: Alignment.centerLeft,
                                height: deviceSize.height*0.1,
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).primaryColor),
                                  decoration: InputDecoration(
                                    hintText: 'Password',
                                    suffixIcon: IconButton(
                                      onPressed: () => _togglePasswordVisibility(true),
                                      icon: _isPasswordVisible ? const Icon(Icons.visibility_off_outlined) : const Icon(Icons.visibility_outlined),
                                    ),
                                  ),
                                  validator: (value) {
                                    return passwordErrorMessage(value);
                                  },
                                  controller: passwordController,
                                  obscureText: !_isPasswordVisible,
                                )
                            ),
                            Container(
                                alignment: Alignment.centerLeft,
                                height: deviceSize.height*0.1,
                                child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).primaryColor),
                                  decoration: InputDecoration(
                                      hintText: 'Confirm Password',
                                    suffixIcon: IconButton(
                                      onPressed: () => _togglePasswordVisibility(false),
                                      icon: _isConfirmPasswordVisible ? const Icon(Icons.visibility_off_outlined) : const Icon(Icons.visibility_outlined),
                                    ),
                                  ),
                                  validator: (value) {
                                    if(value != passwordController.text) {
                                      return 'Passwords do not match.';
                                    }
                                    return null;
                                  },
                                  obscureText: !_isConfirmPasswordVisible,
                                )
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: (deviceSize.height*0.05+6),
                              ),
                              padding: EdgeInsets.only(bottom: deviceSize.height*0.02),
                              width: deviceSize.width,
                              // height: deviceSize.height*0.1,
                              alignment: Alignment.center,
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  )),
                                  backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).colorScheme.primary),
                                  alignment: Alignment.center,
                                  fixedSize: MaterialStateProperty.all<Size>(Size(deviceSize.width,50))
                                ),
                                onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      _formKey.currentState!.save();
                                      UserData user = UserData(
                                        email: emailController.text,
                                        name: nameController.text,
                                        lat: locationData?.latitude,
                                        lng: locationData?.longitude,
                                        signInMethod: "EMAIL",
                                        createdAt: DateTime.now(),
                                      );
                                      _bloc.registerUser(user, passwordController.text);
                                    }
                                },
                                child: Text(
                                  'Sign Up',
                                  style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white,fontSize: 16),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            _buildSignInWithText(),
                            _buildSocialBtnRow(),
                            _buildLoginInsteadButton(deviceSize),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
  }
  Widget _buildSignInWithText() {
    return Container(
      alignment: Alignment.center,
      child: RichText(
        text: TextSpan(
          text: '- Or Sign Up With -',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey),
        ),
      ),
    );
  }
  Widget _buildSocialBtnRow() {
    final deviceSize = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: deviceSize.height*0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildSocialBtn(
                () {
              AppUtility.googleLogin(context);
            },
            const AssetImage(
              'images/googleLogo.png',
            ),
          ),
          _buildSocialBtn(
                () {
              AppUtility.facebookLogin(context);
            },
            const AssetImage(
              'images/facebookLogo.png',
            ),
          ),
          if(Platform.isIOS)...[
            _buildSocialBtn(
                  () {
                AppUtility.loginWithApple(context);
              },
              const AssetImage(
                'images/apple_logo.png',
              ),
            ),
          ],
        ],
      ),
    );
  }
  Widget _buildSocialBtn(Function() onTap, AssetImage logo) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          image: DecorationImage(
            image: logo,
          ),
        ),
      ),
    );
  }

  Widget _buildLoginInsteadButton(Size deviceSize) {
    return Container(
      margin: EdgeInsets.only(
          top: deviceSize.height*0.02
      ),
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: () => Navigator.of(context).pop(),
        child: RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: 'Already have an Account? ',
                style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey),
              ),
              TextSpan(
                text: 'Log in',
                style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).colorScheme.primary,fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
