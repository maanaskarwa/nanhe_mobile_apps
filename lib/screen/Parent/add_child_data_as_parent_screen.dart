import 'dart:io';
import 'dart:typed_data';

import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image/image.dart' as face_ui;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_event.dart';
import 'package:nanhe/bloc/api_state.dart';
import 'package:nanhe/model/child_data_from_parent.dart';
import 'package:nanhe/model/json_serializable_utils.dart';
import 'package:nanhe/route_generator.dart';
import 'package:nanhe/util/app_constants.dart';
import 'package:nanhe/util/app_utility.dart';

enum screenMode {editing, newEntry}

class AddChildDataAsParentScreen extends StatefulWidget {
  const AddChildDataAsParentScreen({
    Key? key,
    required this.currentScreenMode,
    this.previousData,
  }) : super(key: key);
  final screenMode currentScreenMode;
  final ChildDataFromParent? previousData;

  @override
  _AddChildDataAsParentScreenState createState() => _AddChildDataAsParentScreenState();
}

class _AddChildDataAsParentScreenState extends State<AddChildDataAsParentScreen> {
  late APIBloc bloc;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController dobController = TextEditingController();
  final TextEditingController missingSinceController = TextEditingController();
  final TextEditingController identificationMarkController = TextEditingController();
  Gender? gender;
  DateTime? dateOfBirth;
  DateTime missingSince = DateTime.now();
  List<File> imagesToUpload = [];
  List<face_ui.Image> faceImages = [];
  ChildDataFromParent? prevChildData;
  bool editing = false;

  // below functions to populate and display images of missing child - to be uploaded by parent

  Future<void> getAndAddImage(Size deviceSize) async {
    var img = await AppUtility.getImageFromCameraOrGallery(ImageSource.gallery);
    if(img == null) {
      return;
    } else {
      FaceDetector detector = GoogleMlKit.vision.faceDetector();
      AppUtility.showProgressDialog(context);
      List<Face> faces = await detector.processImage(InputImage.fromFile(File(img.path)));
      Navigator.of(context).pop();
      if(faces.isEmpty) {
        AppUtility.showMessageDialog(
          context,
          '''No face detected in image. Please try again''',
        );
      } else {
        List<face_ui.Image> facePictures = [];
        for(var childFace in faces) {
          face_ui.Image picture = face_ui.copyCrop(
            face_ui.decodeImage(File(img.path).readAsBytesSync())!,
            childFace.boundingBox.left.toInt(),
            childFace.boundingBox.top.toInt(),
            childFace.boundingBox.width.toInt(),
            childFace.boundingBox.height.toInt(),
          );
          facePictures.add(picture);
        }
        final response = await showDialog(
          context: context,
          builder: (context) {
            int? selectedItem;
            return Dialog(
              child: StatefulBuilder(
                builder: (context, notify) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        RichText(
                          text: TextSpan(
                            text: faces.length == 1 ? "Confirm Child's Face" : "Select Child's Face",
                            style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 24,fontWeight: FontWeight.w500),
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Wrap(
                              children: facePictures.map((e) {
                                int index = facePictures.indexOf(e);
                                return InkWell(
                                  onTap: () {
                                    notify(() {
                                      selectedItem = index;
                                    });
                                  },
                                  child: index != selectedItem ? Container(
                                    margin: const EdgeInsets.only(top:10,bottom: 25),
                                    width: deviceSize.width*0.3,
                                    height: 100,
                                    child: FittedBox(child: Image.memory(Uint8List.fromList(face_ui.encodePng(e)))),
                                  ) : Stack(
                                    children: [
                                      Container(
                                        margin: const EdgeInsets.only(bottom: 25,top: 10),
                                        width: deviceSize.width*0.35,
                                        height: 120,
                                        child: FittedBox(child: Image.memory(Uint8List.fromList(face_ui.encodePng(e)))),
                                      ),
                                      const Positioned(
                                        top: 15,
                                        right: 5,
                                        child: CircleAvatar(
                                          backgroundColor: Colors.white,
                                          child: Icon(Icons.check,color: Colors.green,),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }).toList()
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: const Text('Cancel')
                            ),
                            ElevatedButton(
                              onPressed: selectedItem == null? null : () {
                                Navigator.of(context).pop(facePictures[selectedItem!]);
                              },
                              child: const Text('Confirm'),
                            )
                          ],
                        ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
        if(response != null) {
          imagesToUpload.add(File(img.path));
          faceImages.add(response as face_ui.Image);
          setState(() {});
        }
      }
    }
  }

  Future<void> removeImage(face_ui.Image img) async {
    AppUtility.showConfirmDialog(context, 'Remove this image?', () {
      imagesToUpload.removeAt(faceImages.indexOf(img));
      faceImages.removeWhere((element) => element == img);
      setState(() {});
      Navigator.of(context).pop();
    });
  }

  List<Widget> _buildImagesView() {
    return faceImages.map((e) => Expanded(
      child: Stack(children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(10)
          ),
          height: 135,
          margin: const EdgeInsets.only(top: 25, left: 4.5, right: 4.5),
          alignment: Alignment.center,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.memory(
              Uint8List.fromList(face_ui.encodePng(e)),
              height: 135,
              fit: BoxFit.scaleDown,
              alignment: Alignment.center,
            ),
          ),
        ),
        Positioned(
          child: GestureDetector(
              child: const CircleAvatar(
                radius: 12,
                backgroundColor: Colors.black,
                child: Icon(
                  Icons.cancel_outlined,
                  color: Colors.white,
                ),
              ),
              onTap: () => removeImage(e)),
          top: 30,
          right: 10,
        ),
      ]),
    )).toList();
  }

  Widget _buildPickImagesContainer(Size deviceSize, int scaleFactor, String text,double fontSize) {
    return Container(
      height: 135,
      width: (deviceSize.width-40-(scaleFactor == 1 ? 0 : scaleFactor == 2 ? 9 : 18))/scaleFactor,
      margin: EdgeInsets.only(
        top: 25,
        right: scaleFactor > 1 ? 4.5: 0,
        left: scaleFactor > 1 ? 4.5: 0,
      ),
      alignment: Alignment.center,
      child: DottedBorder(
        borderType: BorderType.RRect,
        radius: const Radius.circular(10),
        color: Theme.of(context).colorScheme.primary,
        strokeWidth: 2,
        dashPattern: const [5],
        child: InkWell(
          onTap: () => getAndAddImage(deviceSize),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 22.5,
                backgroundColor: Theme.of(context).colorScheme.primary,
                child: const Icon(
                  Icons.add_photo_alternate,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 20,
                width: deviceSize.width,
              ),
              RichText(
                text: TextSpan(
                  text: text,
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    fontSize: fontSize,
                  ),
                ),
              ),
              if(text == 'Add up to 3 pictures') RichText(
                text: TextSpan(
                  text: '(Ideally front, left, and right view)',
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey[600])
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  // combines above widgets
  Widget _buildImagesStuff(Size deviceSize) {
    if(imagesToUpload.isEmpty) {
      return _buildPickImagesContainer(deviceSize, 1, 'Add up to 3 pictures', 16);
    }
    else if(imagesToUpload.length == 1) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ..._buildImagesView(),
          _buildPickImagesContainer(deviceSize, 2, 'Add more images', 12),
        ],
      );
    }
    else if (imagesToUpload.length == 2) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ..._buildImagesView(),
          _buildPickImagesContainer(deviceSize, 3, 'Add more images', 10),
        ],
      );
    }
    else if (imagesToUpload.length == 3) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildImagesView(),
      );
    }
    return Container();
  }

  @override
  void initState() {
    bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
    missingSinceController.text = DateFormat(Constants.displayDateFormat).format(missingSince);
    if(widget.currentScreenMode == screenMode.editing) {
      prevChildData = widget.previousData;
      Future.delayed(Duration.zero).then((_) {
        setState(() {
          nameController.text = prevChildData!.name;
          dateOfBirth = prevChildData!.dob;
          dobController.text = DateFormat(Constants.displayDateFormat).format(dateOfBirth!);
          identificationMarkController.text = prevChildData!.identificationMark!;
          missingSince = prevChildData!.missingSince;
          missingSinceController.text = DateFormat(Constants.displayDateFormat).format(missingSince);
          gender = prevChildData!.gender;
          editing = true;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String uid = FirebaseAuth.instance.currentUser!.uid;
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.black,
        actions: const [],
      ),
      body: SafeArea(
        child: BlocListener(
          bloc: bloc,
          listener: (BuildContext context, state) {
            if (state is APIErrorState) {
              if (state.event is UploadChildDataAsParentEvent) {
                Navigator.pop(context);
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(state.error)));
              }
            }
            if (state is APILoadingState) {
              AppUtility.showProgressDialog(context);
            }
            if (state is APILoadedState<ChildDataFromParent>) {
              if (state.event is UploadChildDataAsParentEvent) {
                Navigator.pop(context);
                Navigator.of(context).pushReplacementNamed(RoutesDirectory.parentAfterUpload,arguments: state.data);
              } else if (state.event is UpdateChildDataAsParentEvent) {
                Navigator.pop(context);
                AppUtility.showMessageDialog(
                  context,
                  """Child data updated successfully. 
Thank You. """,
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  }
                );
              }
            }
          },
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: editing ? 'Missing Child Info' :'Report Missing Child',
                          style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 31,fontWeight: FontWeight.w500),
                        ),
                      ),
                      if (editing) ...[
                      RichText(
                        text: TextSpan(
                          text: 'If you wish to change uploaded pictures, mark this child as inactive and upload new data.',
                          style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 16,fontStyle: FontStyle.italic)
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            buildMarkAsFoundButton(prevChildData!,deviceSize),
                            buildMarkAsInactiveButton(prevChildData!,deviceSize),
                          ],
                        ),
                      ),
                      ] else Container(child: _buildImagesStuff(deviceSize),margin: const EdgeInsets.only(bottom: 15),),
                      TextFormField(
                        controller: nameController,
                        decoration: InputDecoration(
                          label: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "Child's Name",
                                  style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey,fontSize: 16),
                                ),
                                TextSpan(
                                  text: '*',
                                  style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).colorScheme.error,fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter your child\'s name';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(
                        height:10
                      ),
                      TextFormField(
                        controller: identificationMarkController,
                        decoration: InputDecoration(
                          label: RichText(
                            text: TextSpan(
                              text: 'Identification Mark',
                              style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey,fontSize: 16),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () async {
                          FocusScope.of(context).requestFocus(FocusNode());
                          //setting age limit of 18 on date of birth picker. Different for different countries?
                          DateTime? dt = await AppUtility.getDateFromDatePicker(context,firstDate: DateTime.now().subtract(const Duration(days: 365*18)));
                          if (dt != null) {
                            setState(() {
                              dateOfBirth = dt;
                              dobController.text = DateFormat(Constants.displayDateFormat).format(dt);
                            });
                          }
                        },
                        child: AbsorbPointer(
                          absorbing: true,
                          child: TextFormField(
                            controller: dobController,
                            decoration: InputDecoration(
                              suffixIcon: const Icon(Icons.date_range),
                              label: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: 'Date of birth',
                                      style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey,fontSize: 16),
                                    ),
                                    TextSpan(
                                      text: '*',
                                      style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).colorScheme.error,fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            validator: (value) {
                              if (value == null) {
                                return 'Please enter your child\'s date of birth.';
                              } else if (value.isEmpty) {
                                return 'Please enter your child\'s date of birth.';
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () async {
                          FocusScope.of(context).requestFocus(FocusNode());
                          // setting 2 years limit for missing since picker
                          DateTime? dt = await AppUtility.getDateFromDatePicker(context,firstDate: DateTime.now().subtract(const Duration(days: 730)));
                          if (dt != null) {
                            setState(() {
                              missingSince = dt;
                              missingSinceController.text = DateFormat(Constants.displayDateFormat).format(dt);
                            });
                          }
                        },
                        child: AbsorbPointer(
                          absorbing: true,
                          child: TextFormField(
                            controller: missingSinceController,
                            decoration: InputDecoration(
                              suffixIcon: const Icon(Icons.date_range),
                              label: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: 'Missing since',
                                      style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey,fontSize: 16),
                                    ),
                                    TextSpan(
                                      text: '*',
                                      style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).colorScheme.error,fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5),
                        width: deviceSize.width,
                        child: FittedBox(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: 'Gender',
                                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600,fontSize: 16)
                                    ),
                                    TextSpan(
                                      text: '*',
                                      style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.red,fontSize: 16)
                                    ),
                                  ],
                                ),
                              ),
                              Radio(
                                value: Gender.male,
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value as Gender;
                                  });
                                }
                              ),
                              GestureDetector(
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Male',
                                      style: Theme.of(context).textTheme.bodyText2//.copyWith(fontWeight: FontWeight.w600,fontSize: 16)
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    gender = Gender.male;
                                  });
                                },
                              ),
                              SizedBox(
                                width: deviceSize.width*0.05,
                              ),
                              Radio(
                                value: Gender.female,
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value as Gender;
                                  });
                                }
                              ),
                              GestureDetector(
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Female',
                                      style: Theme.of(context).textTheme.bodyText2//.copyWith(fontWeight: FontWeight.w600,fontSize: 16)
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    gender = Gender.female;
                                  });
                                },
                              ),
                              SizedBox(
                                width: deviceSize.width*0.05,
                              ),
                              Radio(
                                value: Gender.other,
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value as Gender;
                                  });
                                }
                              ),
                              GestureDetector(
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Other',
                                      style: Theme.of(context).textTheme.bodyText2//.copyWith(fontWeight: FontWeight.w600,fontSize: 16)
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    gender = Gender.other;
                                  });
                                },
                              ),
                              // SizedBox(
                              //   width: deviceSize.width*0.04,
                              // ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20,bottom: 20),
                        child: ElevatedButton(
                          style: ButtonStyle(
                            // elevation: MaterialStateProperty.all<double>(5.0),
                            // padding: MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.all(15.0)),
                              shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              )),
                              backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).colorScheme.primary),
                              alignment: Alignment.center,
                              fixedSize: MaterialStateProperty.all<Size>(Size(deviceSize.width,50))
                          ),
                          onPressed: editing ? () {
                            if (!_formKey.currentState!.validate()) {
                              //no action required here, validator will show error texts
                            } else if (gender == null) {
                              AppUtility.showMessageDialog(context, 'Please select child\'s gender.');
                            } else if (missingSince.isBefore(dateOfBirth!)) {
                              AppUtility.showMessageDialog(context, 'Date since which the child is missing cannot be before date of birth.');
                            } else if (_formKey.currentState!.validate()) {
                              ChildDataFromParent childData = ChildDataFromParent(
                                createdAt: prevChildData!.createdAt,
                                faceImages: prevChildData!.faceImages,
                                found: prevChildData!.found,
                                images: prevChildData!.images,
                                isActive: prevChildData!.isActive,
                                matchesFound: prevChildData!.matchesFound,
                                matchFound: prevChildData!.matchFound,
                                matchingId: prevChildData!.matchingId,
                                id: prevChildData!.id,
                                uid: prevChildData!.uid,
                                updatedAt: DateTime.now(),
                                gender: gender!,
                                dob: dateOfBirth!,
                                identificationMark: identificationMarkController.text,
                                name: nameController.text,
                                missingSince: missingSince,
                              );
                              print('validated');
                              bloc.updateChildDataAsParent(
                                childData.uid,
                                childData,
                              );
                            }
                          } : () {
                            if (!_formKey.currentState!.validate()) {
                              //no action required here, validator will show error texts
                            }
                            else if(imagesToUpload.isEmpty) {
                              AppUtility.showMessageDialog(context, 'Please select images.');
                            }
                            else if (gender == null) {
                              AppUtility.showMessageDialog(context, 'Please select child\'s gender.');
                            } else if (missingSince.isBefore(dateOfBirth!)) {
                              AppUtility.showMessageDialog(context, 'Date since which the child is missing cannot be before date of birth.');
                            } else if (_formKey.currentState!.validate()) {
                              ChildDataFromParent childData = ChildDataFromParent(
                                uid: uid,
                                createdAt: DateTime.now(),
                                gender: gender!,
                                dob: dateOfBirth!,
                                identificationMark: identificationMarkController.text,
                                name: nameController.text,
                                missingSince: missingSince,
                              );
                              print('validated');
                              bloc.uploadChildDataAsParent(
                                uid,
                                childData,
                                {
                                  'images': imagesToUpload,
                                  'faceImages': faceImages,
                                },
                              );
                            }
                          },
                          child: Text(
                            'Upload',
                            style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white,fontSize: 16),
                          ),
                        ),
                      ),
                      // SizedBox(height: 10,)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildMarkAsFoundButton(ChildDataFromParent childData,Size deviceSize) {
    return LimitedBox(
      maxWidth: deviceSize.width*0.4,
      child: TextButton.icon(
        label: RichText(text:TextSpan(text: 'Mark Found',style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).colorScheme.primary))),
        onPressed: () {
          print('in found button');
          var child = childData;
          child.found = true;
          child.updatedAt = DateTime.now();
          bloc.updateChildDataAsParent(child.uid, child);
          print('bloc command executed');
        },
        style: ButtonStyle(
          side: MaterialStateProperty.all<BorderSide>(BorderSide(color: Theme.of(context).colorScheme.primary))
        ),
        icon: Icon(Icons.check,color: Theme.of(context).colorScheme.primary,size: 20,),
      ),
    );
  }

  Widget buildMarkAsInactiveButton(ChildDataFromParent childData,Size deviceSize) {
    return LimitedBox(
      maxWidth: deviceSize.width*0.4,
      child: TextButton.icon(
        onPressed: () {
          var child = childData;
          child.isActive = false;
          child.updatedAt = DateTime.now();
          bloc.updateChildDataAsParent(child.uid, child);
        },
        label: RichText(text:TextSpan(text: 'Mark Inactive',style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).colorScheme.primary))),
        style: ButtonStyle(
            side: MaterialStateProperty.all<BorderSide>(BorderSide(color: Theme.of(context).colorScheme.primary))
        ),
        icon: Icon(Icons.block,color: Theme.of(context).colorScheme.primary,size: 20,),
      ),
    );
  }
}