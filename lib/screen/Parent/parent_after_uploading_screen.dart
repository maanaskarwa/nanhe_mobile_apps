import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_social_content_share/flutter_social_content_share.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:nanhe/model/child_data_from_parent.dart';
import 'package:nanhe/model/json_serializable_utils.dart';
import 'package:nanhe/route_generator.dart';
import 'package:nanhe/util/app_constants.dart';
import 'package:nanhe/util/app_utility.dart';
import 'package:nanhe/widgets/image_view.dart';

class ParentAfterUploadingScreen extends StatelessWidget {
  const ParentAfterUploadingScreen(this.childData,{Key? key,}) : super(key: key) ;
  final ChildDataFromParent childData;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.mainDashboard,(_) => false);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                  text: 'Upload Successful 👍🏽',
                  style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 27,fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 20),
                child: Wrap(
                  alignment: WrapAlignment.spaceEvenly,
                  spacing: 5,
                  crossAxisAlignment: WrapCrossAlignment.end,
                  children: childData.faceImages!.map((e) => LimitedBox(
                    maxHeight: 120,
                    child: ImageView(e, ImageType.url,width: deviceSize.width*0.3, fit: BoxFit.scaleDown,),
                    // margin: const EdgeInsets.only(top: 40,bottom: 20),
                    // alignment: Alignment.center,
                  ),).toList()
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'What we will do',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w600,fontSize: 16)
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 5),
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                          text: '''
  \u2022 Match it with pictures uploaded by 
    volunteers across the country
  \u2022 Inform you when a match happens
''',
                          style: Theme.of(context).textTheme.bodyText2
                      ),
                    ]
                  ),
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'What you need to do',
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 16,fontWeight: FontWeight.w600)
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 5),
                child: RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                            text: '''
  \u2022 Report to police
  \u2022 Visit nanhe daily. If you don't visit for 3 
    days, we pause the matching process
''',
                            style: Theme.of(context).textTheme.bodyText2
                        ),
                      ]
                  ),
                ),
              ),
              Center(
                child: RichText(
                  text: TextSpan(
                    text: 'Spread the Word',
                    style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 27,fontWeight: FontWeight.w500),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children:
                  _buildSocialShareButtons(context,childData),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 30),
                  alignment:Alignment.center,
                  child: TextButton(onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(RoutesDirectory.mainDashboard,(_) => false), child: const Text('Back to Home')))
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _buildSocialShareButtons(BuildContext context,ChildDataFromParent childData) {
    return [
      IconButton(
        onPressed: () async {
          await FlutterSocialContentShare.share(
              type: ShareType.facebookWithoutImage,
              quote: 'My child ${childData.name} is missing since ${DateFormat(Constants.displayDateFormat).format(childData.missingSince)}. Please download the nanhe app from nanhe.org to help me find ${childData.gender == Gender.male? 'him' : childData.gender == Gender.female? 'her' : 'them'}. Here is ${childData.gender == Gender.male? 'his' : childData.gender == Gender.female? 'her' : 'their'} picture.',
              url: childData.images!.first
          );
        },
        icon: const Icon(
          FontAwesomeIcons.facebook,
          color: Color.fromRGBO(59, 89, 152, 1),
          size: 40,
        ),
      ),
      IconButton(
        onPressed: () async {
          await FlutterSocialContentShare.shareOnWhatsapp(
            '0',
            '''My child ${childData.name} is missing since ${DateFormat(Constants.displayDateFormat).format(childData.missingSince)}. Please help me find ${childData.gender == Gender.male?'him' : childData.gender == Gender.female?'her' : 'them'}. I have heard about gangs which kidnap children, move them to another town, and force them into begging or labor 😨

Please use the nanhe app and upload pictures of any child you see begging 🙏

God bless you!

Android: https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app''',
          );
        },
        icon: Icon(
          FontAwesomeIcons.whatsapp,
          color: Color.lerp(const Color.fromRGBO(37, 211, 102, 1), const Color.fromRGBO(18, 140, 126, 1), 0),
          size: 40,
        ),
      ),
      GestureDetector(
        child: Container(
          height: 40,
          width: 40,
          alignment: Alignment.center,
          padding: const EdgeInsets.only(bottom: 2.5),
          decoration:  BoxDecoration(
            borderRadius: BorderRadius.circular(7.5),
            gradient: const LinearGradient(
                colors: [
                  Color(0xffF58529),
                  Color(0xffFEDA77),
                  Color(0xffDD2A7B),
                  Color(0xff8134AF),
                  Color(0xff515BD4),
                ],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight
            ),
          ),
          child: const Icon(
            FontAwesomeIcons.instagram,
            color: Colors.white,
            size: 35,
          ),
        ),
        onTap: () async {
          FlutterClipboard.copy('My child ${childData.name} is missing since ${DateFormat(Constants.displayDateFormat).format(childData.missingSince)}. Please download the nanhe app from nanhe.org to help me find ${childData.gender == Gender.male? 'him' : childData.gender == Gender.female? 'her' : 'them'}. Here is ${childData.gender == Gender.male? 'his' : childData.gender == Gender.female? 'her' : 'their'} picture.');
          AppUtility.showMessageDialog(
              context,
              '''To save your time, we have created the caption for your post. Please paste it in Instagram.
              
It may take a few seconds...''',
              onPressed: () async {
                final response = await FlutterSocialContentShare.share(
                  type: ShareType.instagramWithImageUrl,
                  imageUrl: childData.images!.first,
                );
                Navigator.of(context).pop();
              }
          );
        },
      ),
    ];
  }
}
