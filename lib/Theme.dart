import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final ThemeData appThemeData = ThemeData(
  primarySwatch: const MaterialColor(4278190335, {50: Color(0xff7373ff), 100: Color(0xff6666ff), 200: Color(0xff4d4dff), 300: Color(0xff3333ff), 400: Color(0xff1919ff), 500: Color(0xff0000ff), 600: Color(0xff0000e5), 700: Color(0xff0000cc), 800: Color(0xff0000b2), 900:  Color(0xff000099)}),
  colorScheme: ColorScheme.fromSwatch(
    primarySwatch: const MaterialColor(4278190335, {50: Color(0xff7373ff), 100: Color(0xff6666ff), 200: Color(0xff4d4dff), 300:  Color(0xff3333ff), 400: Color(0xff1919ff), 500: Color(0xff0000ff), 600: Color(0xff0000e5), 700: Color(0xff0000cc), 800:  Color(0xff0000b2), 900: Color(0xff000099)}),
    brightness: Brightness.light,
    accentColor: AppColors.accentColor,
    backgroundColor: Colors.white,
  ),
  hintColor: Colors.grey,
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    backgroundColor: AppColors.primarySwatch,
    unselectedItemColor: AppColors.appBg,
    selectedItemColor: AppColors.accentColor
  ),
  backgroundColor: AppColors.appBg,
  scaffoldBackgroundColor: Colors.white,
  appBarTheme: const AppBarTheme(color: AppColors.primarySwatch, elevation: 0.0),
  textTheme: TextTheme(
    headline6: GoogleFonts.poppins(color: AppColors.appTextDark,fontWeight: FontWeight.w600),
    bodyText2: GoogleFonts.poppins(color: AppColors.appTextDark,fontSize: 14,fontWeight: FontWeight.w400),
    button: GoogleFonts.poppins(color: AppColors.appTextDark,fontWeight: FontWeight.w600),
    bodyText1: GoogleFonts.poppins(color: AppColors.appTextDark,fontWeight: FontWeight.w400),
  ),
  textSelectionTheme: const TextSelectionThemeData(selectionColor: Colors.black),
);

class AppColors {
  AppColors._();

  static const MaterialColor primarySwatch = MaterialColor(4278190335, {50: Color(0xff7373ff), 100: Color(0xff6666ff), 200: Color(0xff4d4dff), 300: Color(0xff3333ff), 400: Color(0xff1919ff), 500: Color(0xff0000ff), 600: Color(0xff0000e5), 700: Color(0xff0000cc), 800: Color(0xff0000b2), 900: Color(0xff000099)});
  static const Color accentColor = Color.fromRGBO(255, 20, 147, 1);
  static const Color appBg = Colors.white;
  static const Color appTextDark = Color.fromRGBO(63, 63, 63, 1.0);
}

void createMaterialColor(Color color) {// useful to convert Color to a swatch
  List<double> strengths = [.05];
  final Map<int, Color> swatch = {};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  for (var strength in strengths) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  }
  print(color.value);
  print(swatch);
}