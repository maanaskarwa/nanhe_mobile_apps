import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppRadioButton<T> extends StatelessWidget {
  final String title;
  final T value;
  final T groupValue;
  final EdgeInsetsGeometry padding;
  final Color titleColor;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextAlign? textAlign;
  final TextDecoration? textDecoration;
  final Function(T)? onChanged;

  const AppRadioButton({
    Key? key,
    required this.title,
    required this.value,
    required this.groupValue,
    this.padding = const EdgeInsets.all(10.0),
    this.titleColor = Colors.black,
    this.fontSize = 14.0,
    this.fontWeight,
    this.textAlign,
    this.textDecoration,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: padding,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              value == groupValue
                  ? Icons.check_circle
                  : Icons.radio_button_unchecked,
              color: value == groupValue ? Theme.of(context).colorScheme.secondary : Colors.black,
            ),
            const SizedBox(
              width: 10,
            ),
            Flexible(
              child: Text(
                title,
                textAlign: textAlign,
                style: GoogleFonts.poppins(
                  color: titleColor,
                  fontSize: fontSize,
                  fontWeight: fontWeight,
                  decoration: textDecoration,
                ),
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        if (onChanged != null) {
          onChanged!(value);
        }
      },
    );
  }
}
