import 'package:flutter/material.dart';

class AppLoader extends StatelessWidget {
  const AppLoader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: const <Widget>[
            SizedBox(
              child: CircularProgressIndicator(),
              height: 80.0,
              width: 80.0,
            )
          ],
        ),
      ),
    );
  }
}
