import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

enum ImageType { asset, file, url }

class ImageView extends StatelessWidget {
  final String path;
  final ImageType type;
  final double? height, width;
  late final String placeHolderImagePath;
  final BoxFit? fit;

  ImageView(this.path, this.type,
      {Key? key, this.height,
      this.width,
      this.placeHolderImagePath = 'images/icon_placeholder.png',
      this.fit}) : super(key: key) {
    if (placeHolderImagePath.isEmpty) {
      placeHolderImagePath = 'images/icon_placeholder.png';
    }
  }

  @override
  Widget build(BuildContext context) {
    return showImage(path, type, height, width, placeHolderImagePath, fit);
  }

  Widget showImage(
    String? path,
    ImageType type,
    double? height,
    double? width,
    String placeHolderImagePath,
    BoxFit? fit,
    ) {
    if (path != null && path.isNotEmpty) {
      switch (type) {
        case ImageType.asset:
          try {
            return ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset(
                path,
                height: height,
                width: width,
                fit: fit,
              ),
            );
          } catch (e) {
            print(e);
            return placeHolder(height, width, placeHolderImagePath, fit);
          }
        case ImageType.file:
          try {
            File f = File(path);
            if (f.existsSync()) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.file(
                  f,
                  height: height,
                  width: width,
                  fit: fit,
                ),
              );
            } else {
              return placeHolder(height, width, placeHolderImagePath, fit);
            }
          } catch (e) {
            print(e);
            return placeHolder(height, width, placeHolderImagePath, fit);
          }
        case ImageType.url:
          try {
            return ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                imageUrl: path,
                placeholder: (context, url) => placeHolder(height, width, placeHolderImagePath, fit),
                errorWidget: (context, url, error) => placeHolder(height, width, placeHolderImagePath, fit),
                height: height,
                fit: fit,
              ),
            );
          } catch (e) {
            print(e);
            return placeHolder(height, width, placeHolderImagePath, fit);
          }
      }
    }
    return placeHolder(height, width, placeHolderImagePath, fit);
  }

  Widget placeHolder(double? height, double? width, String placeHolderImagePath, BoxFit? fit) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Image.asset(
        placeHolderImagePath,
        height: height,
        width: width,
        fit: fit,
      ),
    );
  }
}
