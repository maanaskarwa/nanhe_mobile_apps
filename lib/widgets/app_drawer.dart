import 'dart:io' as platform show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:nanhe/bloc/api_bloc.dart';
import 'package:nanhe/bloc/api_event.dart';
import 'package:nanhe/bloc/api_state.dart';
import 'package:nanhe/util/data_repository.dart' show UserDataRepository ;
import 'package:url_launcher/url_launcher.dart';

import '../model/user_data.dart';
import '../route_generator.dart' show RoutesDirectory;
import '../util/app_utility.dart';
import '../widgets/text_view.dart';

enum CurrentScreen {
  home,
  howItWorks,
  none
}

class AppDrawer extends StatefulWidget {
  AppDrawer(this._currentScreen,{Key? key}) : super(key: key);

  CurrentScreen _currentScreen;
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  late final APIBloc bloc;
  late UserData userdata;

  @override
  void initState() {
    userdata = UserDataRepository.getUserData!;
    bloc = BlocProvider.of<APIBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final deviceSize = MediaQuery.of(context).size;
    return BlocListener(
      bloc: bloc,
      listener: (BuildContext context, state) {
        if (state is APILoadingState) {
          AppUtility.showProgressDialog(context);
        }
        if (state is APIErrorState) {
          print('event ${state.event}');
        }
        if (state is APILoadedState) {
          print('event ${state.event}');
          if (state.event is LogOutEvent) {
            if (state.data) {
              Navigator.pop(context);
              Navigator.pushNamedAndRemoveUntil(context, RoutesDirectory.splash, (route) => false,arguments: false);
            }
          }
        }
      },
      child: Drawer(
        child: Column(
          children: [
            AppBar(
              automaticallyImplyLeading: false,
              title: ListTile(
                title: TextView(userdata.name,fontSize: 18,textColor: Colors.white,),
                // trailing: IconButton(
                //   icon: Icon(
                //     Icons.edit,
                //     color: Colors.white,
                //   ),
                //   onPressed: (){}, //TODO: Open Dialog showing user credentials and textfields to enter
                // ),
              ),
            ),
            const Divider(thickness: 0.6,color: Colors.transparent,),
            ListTile(
              leading: const Icon(Icons.home),
              title: const Text('Home'),
              onTap: () {
                Navigator.of(context).pop();
                if(widget._currentScreen != CurrentScreen.home) {
                  setState(() {
                    widget._currentScreen = CurrentScreen.home;
                  });
                  Navigator.of(context).pushReplacementNamed(RoutesDirectory.mainDashboard,arguments: userdata);
                }
              },
            ),
            const Divider(thickness: 0.6,),
            ListTile(
              leading: const Icon(Icons.help),
              title: const Text('How it works'),
              onTap: () {
                Navigator.of(context).pop();
                if (widget._currentScreen != CurrentScreen.howItWorks) {
                  setState(() {
                    widget._currentScreen = CurrentScreen.howItWorks;
                  });
                  Navigator.of(context).pushNamed(RoutesDirectory.howItWorks,arguments: false);
                }
              },
            ),
            const Divider(thickness: 0.6,),
            ListTile(
              leading: const Icon(FontAwesomeIcons.globe,size: 21,),
              title: const Text('Visit our website'),
              onTap: () async {
                String _url = 'https://www.nanhe.org/';
                await canLaunch(_url) ? await launch(_url) : AppUtility.showMessageDialog(context, '''An error occurred.''');
              },
            ),
            if(platform.Platform.isAndroid) ...[
              const Divider(thickness: 0.6,),
              ListTile(
                leading: const Icon(Icons.star_outline_rounded,color: Colors.grey,),
                title: const Text('Rate the nanhe app'),
                onTap: () async {
                  if(platform.Platform.isAndroid) {
                    await canLaunch('https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app') ? await launch('https://play.google.com/store/apps/details?id=com.nanhe.nanhe_app'):AppUtility.showMessageDialog(context, 'error');
                  }
                },
              ),
            ],
            const Spacer(),
            const Divider(thickness: 0.6,color: Colors.transparent,),
            ListTile(
              leading: Icon(Icons.exit_to_app,color: Theme.of(context).errorColor,),
              title: Text(
                'Log Out',
                style: TextStyle(
                  color: Theme.of(context).errorColor,
                ),
              ),
              onTap: () async {
                if (userdata.signInMethod == "GOOGLE") {
                  await GoogleSignIn().signOut();
                } else if (userdata.signInMethod == "FACEBOOK") {
                 await FacebookAuth.instance.logOut();
                }
                bloc.logoutUser();
              },
            ),
          ],
        ),
      ),
    );
  }
}
