import 'package:flutter/material.dart';

class NoDataView extends StatelessWidget {
  final BuildContext context;
  final String noDataText;

  const NoDataView(this.context, {Key? key,this.noDataText = 'No data found'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: noDataText,
            style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.grey)
          )
        ),
      ),
    );
  }
}
