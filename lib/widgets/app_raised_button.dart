import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class AppRaisedButton extends StatelessWidget {
  final String? title;
  final Widget? child;
  final Color? color;
  final double height;
  final double? width;
  final double borderRadius;
  final Color titleColor;
  final double titleSize;
  final FontWeight titleFontWeight;
  final Function()? onPressed;
  final Color borderColor;
  final double elevation;

  const AppRaisedButton({
    Key? key,
    this.title,
    this.child,
    this.color,
    this.height = 40.0,
    this.width,
    this.elevation = 0,
    this.borderRadius = 0,
    this.titleColor = Colors.white,
    this.titleSize = 14.0,
    this.titleFontWeight = FontWeight.bold,
    this.onPressed,
    this.borderColor = Colors.transparent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(color ?? Theme.of(context).colorScheme.primary),
          elevation: MaterialStateProperty.all<double>(elevation),
          shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
            side: BorderSide(color: borderColor),
            borderRadius: BorderRadius.circular(borderRadius))
          ),
        ),
        child: title != null ? Text(
          title!,
          style: GoogleFonts.poppins(
            color: titleColor,
            fontSize: titleSize,
            fontWeight: titleFontWeight,
          ),
        ) : child,
        onPressed: onPressed
      ),
    );
  }
}
