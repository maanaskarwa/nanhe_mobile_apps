import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TextView extends StatelessWidget {
  final String data;
  final TextAlign? textAlign;
  final TextOverflow textOverflow;
  final int? maxLines;
  final Color? textColor;
  final Color? backgroundColor;
  final double fontSize;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final double? letterSpacing;
  final double? wordSpacing;
  final List<Shadow>? shadows;
  final TextDecoration? textDecoration;
  final double? height;

  const TextView(this.data,
      {Key? key,
        this.textAlign,
        this.textOverflow = TextOverflow.visible,
        this.maxLines,
        this.textColor,
        this.backgroundColor,
        this.fontSize = 14.0,
        this.fontWeight,
        this.fontStyle,
        this.letterSpacing,
        this.wordSpacing,
        this.shadows,
        this.textDecoration,
        this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      textAlign: textAlign,
      overflow: textOverflow,
      maxLines: maxLines,
      style: GoogleFonts.poppins(
        color: textColor,
        backgroundColor: backgroundColor,
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontStyle: fontStyle,
        letterSpacing: letterSpacing,
        wordSpacing: wordSpacing,
        shadows: shadows,
        decoration: textDecoration,
        height: height,
      ),
    );
  }
}
