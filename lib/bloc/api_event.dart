import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:nanhe/model/child_data_from_parent.dart';
import 'package:nanhe/model/child_data_from_volunteer.dart';
import 'package:nanhe/model/user_data.dart';

@immutable
abstract class APIEvent extends Equatable {
  const APIEvent([List props = const []]) : super();
}

class SignInWithEmailEvent extends APIEvent {
  final String email;
  final String password;

  SignInWithEmailEvent(this.email, this.password) : super([email, password]);

  @override
  String toString() => 'SignInWithEmailEvent';

  @override
  List<Object> get props => throw UnimplementedError();
}

class GetUserDataEvent extends APIEvent {
  final String uid;

  GetUserDataEvent(this.uid) : super([uid]);

  @override
  String toString() => 'GetUserDataEvent';

  @override
  List<Object> get props => throw UnimplementedError();
}

class LogOutEvent extends APIEvent {
  const LogOutEvent() : super();

  @override
  String toString() => 'GetUserDataEvent';

  @override
  List<Object?> get props => throw UnimplementedError();
}

class GetCurrentUserEvent extends APIEvent {
  const GetCurrentUserEvent() : super();

  @override
  String toString() => 'GetCurrentUserEvent';

  @override
  List<Object> get props => throw UnimplementedError();
}

class RegisterUserEvent extends APIEvent {
  final UserData userData;
  final String password;

  RegisterUserEvent(this.userData, this.password) : super([userData, password]);

  @override
  String toString() => 'RegisterUserEvent';

  @override
  List<Object> get props => throw UnimplementedError();
}

class UploadChildDataAsVolunteerEvent extends APIEvent {
  final Map<String,dynamic> childImages;
  final ChildDataFromVolunteer childData;

  UploadChildDataAsVolunteerEvent(this.childImages, this.childData) : super([childImages, childData]);

  @override
  String toString() => 'UploadChildDataAsVolunteerEvent';

  @override
  List<Object> get props => throw UnimplementedError();
}

class UploadChildDataAsParentEvent extends APIEvent {
  final String userId;
  final Map<String,dynamic> images;
  final ChildDataFromParent childData;

  UploadChildDataAsParentEvent(this.userId, this.images, this.childData) : super([userId, images, childData]);

  @override
  String toString() => 'UploadChildDataAsParentEvent';

  @override
  List<Object> get props => throw UnimplementedError();
}

class UpdateChildDataAsParentEvent extends APIEvent {
  final String userId;
  final ChildDataFromParent childData;

  UpdateChildDataAsParentEvent(this.userId, this.childData) : super([userId, childData]);

  @override
  String toString() => 'UpdateChildDataAsParentEvent';

  @override
  List<Object> get props => throw UnimplementedError();
}
