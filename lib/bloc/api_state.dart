import 'package:flutter/material.dart';

import 'api_event.dart';

@immutable
abstract class APIState {
  const APIState([List props = const []]) : super();
}

class InitialAPIState extends APIState {}

class APILoadingState extends APIState {
  final APIEvent event;
  APILoadingState(this.event) : super([event]);
}

class APIErrorState extends APIState {
  final APIEvent event;
  final String error;

  APIErrorState(this.event, this.error) : super([event, error]);
}

class APILoadedState<T> extends APIState {
  final APIEvent event;
  final T data;

  APILoadedState(this.event, this.data) : super([event, data]);
}