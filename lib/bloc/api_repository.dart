import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart' show XFile;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image/image.dart' as face_ui;
import 'package:nanhe/model/child_data_from_parent.dart';
import 'package:nanhe/model/child_data_from_volunteer.dart';
import 'package:nanhe/model/user_data.dart';
import 'package:nanhe/util/auth_response.dart';

class APIRepository {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance; //to CRUD documents
  final firebaseStorageInstance = FirebaseStorage.instance.ref(); // to CRUD imges

  Future<AuthResponse> signInWithEmail(String email, String password) async {
    dynamic error;
    try {
      UserCredential credential = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
      if (credential.user != null) {
        UserData? userdata = await getUserData(credential.user!.uid);
        return AuthResponse.fromData(firebaseUserCredential: credential, userData: userdata);
      }
    } catch (e) {
      error = e;
    }
    return AuthResponse.fromData(error: error);
  }

  Future<UserData?> getUserData(String uid) async {
    try {
      final _query = firestoreInstance.collection('users').doc(uid);
      var snapshot = await _query.get();
      if(snapshot.data() != null) {
        if (snapshot.data()!.isNotEmpty) {
          return UserData.fromJson(snapshot.data() as Map<String,dynamic>);
        }
      }
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<User?> get getCurrentUser async {
    try {
      User? firebaseUser = _firebaseAuth.currentUser;
      if (firebaseUser != null) {
        return firebaseUser;
      }
    } catch (e) {
      print(e);
    }
    return null;
  }

  /// displaying on home screen
  Future<bool> incrementVolunteerCount() async {
    try {
      final summaryStatsData = await firestoreInstance.collection('summary').doc('stats').get()..data();
      await firestoreInstance.collection('summary').doc('stats').update({'volunteerCount': ((summaryStatsData['volunteerCount'] as int) + 1)},);
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  /// displaying on home screen
  Future<bool> incrementParentCount() async {
    try {
      final summaryStatsData = (await firestoreInstance.collection('summary').doc('stats').get()).data();
      await firestoreInstance.collection('summary').doc('stats').update({'parentCount': ((summaryStatsData!['parentCount'] as int) + 1)},);
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  /// displaying on home screen
  Future<bool> incrementVolunteerPictureCount(int pictures) async {
    try {
      final summaryStatsData = await firestoreInstance.collection('summary').doc('stats').get()..data();
      await firestoreInstance.collection('summary').doc('stats').update({'volunteerPictureCount': ((summaryStatsData['volunteerPictureCount'] as int) + pictures)},);
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  /// displaying on home screen
  Future<bool> incrementParentPictureCount() async {
    try {
      final summaryStatsData = (await firestoreInstance.collection('summary').doc('stats').get()).data();
      await firestoreInstance.collection('summary').doc('stats').update({'parentPictureCount': ((summaryStatsData!['parentPictureCount'] as int) + 1)},);
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<UserData> updateUser(UserData userdata, {File? profilePic}) async {
    try {
      DocumentReference _userDocReference = firestoreInstance.collection('users').doc(userdata.uid);
      await _userDocReference.set(
        userdata.toJson(),
        SetOptions(merge: true),
      );
      userdata = (await getUserData(userdata.uid!))!;
      return userdata;
    } catch (e) {
      print(e);
      print('error in updating user');
      return userdata;
    }
  }

  Future<AuthResponse> registerUser(UserData userData, String password) async {
    dynamic error;
    try {
      UserCredential credential = await _firebaseAuth.createUserWithEmailAndPassword(
        email: userData.email,
        password: password,
      );
      if (credential.user != null) {
        await credential.user!.sendEmailVerification();
        userData.uid = credential.user!.uid;
        userData.createdAt = DateTime.now();
        UserData response = await updateUser(userData);
        return AuthResponse.fromData(firebaseUserCredential: credential, userData: response);
      }
    } catch (e) {
      error = e;
    }
    return AuthResponse.fromData(error: error);
  }

  Future<bool> signOut() async {
    try {
      await _firebaseAuth.signOut();
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<ChildDataFromVolunteer?> uploadChildDataAsVolunteer(
    XFile imageByVolunteer,
    face_ui.Image faceImage,
    ChildDataFromVolunteer childDataFromVolunteer,
  ) async {
    String? imgUrl;
    String? faceImgUrl;
    try {
      String userId = childDataFromVolunteer.idOfVolunteerWhoUploaded;
      DocumentReference documentReference = firestoreInstance
        .collection('users')
        .doc(userId)
        .collection('childrenAsVolunteer')
        .doc();
      childDataFromVolunteer.id = documentReference.id;
      TaskSnapshot snapshot = await firebaseStorageInstance
        .child('users/$userId/imagesAsVolunteer/${childDataFromVolunteer.id}/face')
        .child('face.jpg')
        .putData(Uint8List.fromList(face_ui.encodeJpg(faceImage, quality: 100)), SettableMetadata(contentType: 'image/jpg'));
      if (snapshot.state == TaskState.success) {
        faceImgUrl = await snapshot.ref.getDownloadURL();
      }
      final imageByVolunteerAsBytes = await File(imageByVolunteer.path).readAsBytes();
      final face_ui.Image? image = face_ui.decodeImage(imageByVolunteerAsBytes);
      if(image != null) {
        // compressing full image by 60%
        snapshot = await firebaseStorageInstance
          .child('users/$userId/imagesAsVolunteer/${childDataFromVolunteer.id}')
          .child('image.jpg')
          .putData(Uint8List.fromList(face_ui.encodeJpg(image, quality: 60)), SettableMetadata(contentType: 'image/jpg'));
        if (snapshot.state == TaskState.success) {
          imgUrl = await snapshot.ref.getDownloadURL();
        }
      }
      childDataFromVolunteer.images = [imgUrl ?? ''];
      childDataFromVolunteer.faceImages = [faceImgUrl ?? ''];
      await documentReference.set(childDataFromVolunteer.toJson(), SetOptions(merge: true));
      incrementVolunteerPictureCount(1);
      return childDataFromVolunteer;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<ChildDataFromParent?> uploadChildDataAsParent(
      {
    required List<face_ui.Image> faceImages,
    required List<File> fullImages,
    required ChildDataFromParent childData,
    required String userId,
  }) async {
    List<String> imgUrls = [];
    List<String> faceImgUrls = [];
    try {
      DocumentReference documentReference = firestoreInstance
        .collection('users')
        .doc(userId)
        .collection('childrenAsParent')
        .doc();
      childData.id = documentReference.id;
      for (var element in faceImages) {
        int index = faceImages.indexOf(element);
        TaskSnapshot snapshot = await firebaseStorageInstance
          .child('users/$userId/imagesAsParent/${childData.id}/face')
          .child('face${index+1}.jpg')
          .putData(Uint8List.fromList(face_ui.encodeJpg(element, quality: 100)), SettableMetadata(contentType: 'image/jpg'));
        if (snapshot.state == TaskState.success) {
          String url = await snapshot.ref.getDownloadURL();
          faceImgUrls.add(url);
        }
      }
      for (var element in fullImages) {
        int index = fullImages.indexOf(element);
        final imageAsBytes = await element.readAsBytes();
        final face_ui.Image? image = face_ui.decodeImage(imageAsBytes);
        if(image != null) {
          TaskSnapshot snapshot = await firebaseStorageInstance
            .child('users/$userId/imagesAsParent/${childData.id}')
            .child('image${index+1}.jpg')
            .putData(Uint8List.fromList(face_ui.encodeJpg(image, quality: 60)), SettableMetadata(contentType: 'image/jpg'));
          if (snapshot.state == TaskState.success) {
            final url = await snapshot.ref.getDownloadURL();
            imgUrls.add(url);
          }
        }
      }
      childData.images = imgUrls;
      childData.faceImages = faceImgUrls;
      await documentReference.set(childData.toJson(),SetOptions(merge: true));
      incrementParentPictureCount();
      return childData;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<ChildDataFromParent?> updateChildDataAsParent(
    ChildDataFromParent childData,
    String userId,
    ) async {
    try {
      DocumentReference documentReference = firestoreInstance
        .collection('users')
        .doc(userId)
        .collection('childrenAsParent')
        .doc(childData.id);
      await documentReference.update(
        childData.toJson(),
      );
      return childData;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Stream<DocumentSnapshot?> get getSummaryStats {
    final documentSnapShot = firestoreInstance.collection('summary').doc('stats').get().asStream();
    return documentSnapShot;
  }

  Stream<QuerySnapshot> get getChildrenUploadedByAllVolunteers {
    Stream<QuerySnapshot> streamSnapshot = firestoreInstance
      .collection('users')
      .firestore
      .collectionGroup('childrenAsVolunteer')
      .get()
      .asStream();
    return streamSnapshot;
  }

  Stream<QuerySnapshot> getChildrenUploadedByVolunteerByUserId(String userId) {
    Stream<QuerySnapshot> streamSnapshot = firestoreInstance
      .collection('users')
      .doc(userId)
      .collection('childrenAsVolunteer')
      .get()
      .asStream();
    return streamSnapshot;
  }

  Stream<QuerySnapshot> getChildrenUploadedByAllParents([bool missingOnly = false]) {
    Stream<QuerySnapshot> streamSnapshot;
    if(missingOnly) {
      streamSnapshot = firestoreInstance
        .collection('users')
        .firestore
        .collectionGroup('childrenAsParent')
        .where('found',isEqualTo: false)
        .get()
        .asStream();
    } else {
      streamSnapshot = firestoreInstance
        .collection('users')
        .firestore
        .collectionGroup('childrenAsParent')
        .get()
        .asStream();
    }
    return streamSnapshot;
  }

  Stream<QuerySnapshot> getChildrenUploadedByParentByUserId(String userId) {
    Stream<QuerySnapshot> query = firestoreInstance
      .collection('users')
      .doc(userId)
      .collection('childrenAsParent')
      .get()
      .asStream();
    return query;
  }

  Future<void> updateRecentParentPictures(ChildDataFromParent childData) async {
    var data = (await firestoreInstance.collection('summary').doc('recentParentPictures').get()).data();
    data!['old'] = data['new'];
    data['new'] = childData.toJson();
    await firestoreInstance.collection('summary').doc('recentParentPictures').set(data);
  }

  Future<String> get getYouTubeVideoId async {
    try {
      var data = (await firestoreInstance.collection('summary').doc('youtubeVideoId').get()).data();
      return data!['id'];
    } catch (e) {
      print(e);
    }
    return '';
  }
}