import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:camera/camera.dart' show XFile;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:image/image.dart' as face_ui;
import 'package:nanhe/model/child_data_from_parent.dart';
import 'package:nanhe/model/child_data_from_volunteer.dart';
import 'package:nanhe/model/user_data.dart';
import 'package:nanhe/util/app_constants.dart';
import 'package:nanhe/util/auth_response.dart';
import 'package:nanhe/util/data_repository.dart' show UserDataRepository ;

import 'api_event.dart';
import 'api_repository.dart';
import 'api_state.dart';

class APIBloc extends Bloc<APIEvent, APIState> {
  APIRepository apiRepository;

  APIBloc({required this.apiRepository}): super(InitialAPIState());

  void signInWithEmail(String email, String password) {
    add(SignInWithEmailEvent(email, password));
  }

  void getUserData(String uid) {
    add(GetUserDataEvent(uid));
  }

  void getCurrentUser() {
    add(const GetCurrentUserEvent());
  }
  void registerUser(UserData userData, String password) {
    add(RegisterUserEvent(userData, password));
  }

  void logoutUser() {
    add(const LogOutEvent());
  }

  void uploadChildDataAsVolunteer({required Map<String,dynamic> childImages, required ChildDataFromVolunteer childData,}) {
    add(UploadChildDataAsVolunteerEvent(childImages, childData));
  }

  void uploadChildDataAsParent(String userId, ChildDataFromParent childData, Map<String,dynamic> images) {
    add(UploadChildDataAsParentEvent(userId, images, childData));
  }

  void updateChildDataAsParent(String userId, ChildDataFromParent childData) {
    add(UpdateChildDataAsParentEvent(userId, childData));
  }

  @override
  Stream<APIState> mapEventToState(APIEvent event) async* {
    if (event is SignInWithEmailEvent) {
      yield APILoadingState(event);
      AuthResponse response = await apiRepository.signInWithEmail(event.email, event.password);
    if (response.success) {
      yield APILoadedState<UserCredential>(event,response.firebaseUserCredential as UserCredential);
    } else {
      yield APIErrorState(event,response.error as String);
    }

    }
    if (event is GetUserDataEvent) {
      yield APILoadingState(event);
      UserData? response = await apiRepository.getUserData(event.uid);
      if (response != null) {
        yield APILoadedState<UserData>(event,response);
      } else {
        yield APIErrorState(event,Constants.errorSomethingWentWrong);
      }
    }
    if (event is GetCurrentUserEvent) {
      yield APILoadingState(event);
      User? response = await apiRepository.getCurrentUser;
      if (response != null) {
        yield APILoadedState<User>(event,response);
      } else {
        yield APIErrorState(event,Constants.errorSomethingWentWrong);
      }
    }
    if (event is RegisterUserEvent) {
      yield APILoadingState(event);
      print('started');
      AuthResponse response = await apiRepository.registerUser(event.userData, event.password);
      if (response.success) {
        print('done');
        yield APILoadedState<UserCredential>(event,response.firebaseUserCredential as UserCredential);
      } else {
        print('error');
        yield APIErrorState(event,response.error as String);
      }
    }
    if (event is LogOutEvent) {
      yield APILoadingState(event);
      bool response = await apiRepository.signOut();
      if (response) {
        yield APILoadedState<bool>(event,response);
      } else {
        yield APIErrorState(event,Constants.errorSomethingWentWrong);
      }
    }
    if (event is UploadChildDataAsVolunteerEvent) {
      yield APILoadingState(event);
      ChildDataFromVolunteer? response = await apiRepository.uploadChildDataAsVolunteer(
        event.childImages['image'] as XFile,
        event.childImages['faceImage'] as face_ui.Image,
        event.childData,
      );
      if (response != null) {
        yield APILoadedState<ChildDataFromVolunteer>(event,response);
      } else {
        yield APIErrorState(event,Constants.errorSomethingWentWrong);
      }
    }
    if (event is UploadChildDataAsParentEvent) {
      yield APILoadingState(event);
      ChildDataFromParent? response = await apiRepository.uploadChildDataAsParent(
        faceImages:  event.images['faceImages'],
        fullImages:  event.images['images'],
        childData: event.childData,
        userId: event.userId
      );
      UserData? parentData = UserDataRepository.getUserData;
      if(parentData!.isParent == null) {
        parentData.isParent = true;
        apiRepository.incrementParentCount();
        parentData.updatedAt = DateTime.now();
        apiRepository.updateUser(parentData);
      }
      if (response != null) {
        yield APILoadedState<ChildDataFromParent>(event,response);
      } else {
        yield APIErrorState(event,Constants.errorSomethingWentWrong);
      }
    }
    if(event is UpdateChildDataAsParentEvent) {
      yield APILoadingState(event);
      ChildDataFromParent? response = await apiRepository.updateChildDataAsParent(event.childData, event.userId);
      if(response != null) {
        yield APILoadedState(event, response);
      } else {
        yield APIErrorState(event,Constants.errorSomethingWentWrong);
      }
    }
  }
}
