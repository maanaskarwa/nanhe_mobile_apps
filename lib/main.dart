import 'dart:convert';

import 'package:camera/camera.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'bloc/api_bloc.dart';
import 'bloc/api_repository.dart';
import 'route_generator.dart' show RouteGenerator;
import 'theme.dart';
import 'util/app_constants.dart';
import 'util/cameras.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp>
{
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  final APIRepository apiRepository = APIRepository();

  @override
  void initState() {
    super.initState();
    availableCameras().then((value) {
      Cameras.init(value);
    });
    initLocalNotification();
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<APIRepository>(
          create: (context) => apiRepository,
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<APIBloc>(
            create: (context) => APIBloc(apiRepository: apiRepository),
          ),
        ],
        child: MaterialApp(
          title: Constants.appName,
          theme: appThemeData,
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          onGenerateRoute: RouteGenerator.generateRoute,
        ),
      ),
    );
  }

  void initLocalNotification() {
    var initializationSettingsAndroid = const AndroidInitializationSettings('images/n.png');
    var initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: (id, title, body, payload) async {});
    var initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS,);
    flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: (payload) async {});
  }

  Future<void> showLocalNotification(String title, String msg, dynamic data) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      Constants.appName,
      Constants.appName,
      importance: Importance.max,
      priority: Priority.high,
    );
    var iOSPlatformChannelSpecifics = const IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);
    String payload;
    if (data is String) {
      payload = data;
    } else {
      payload = json.encode(data);
    }
    await flutterLocalNotificationsPlugin.show(0, title, msg, platformChannelSpecifics, payload: payload);
  }
}
