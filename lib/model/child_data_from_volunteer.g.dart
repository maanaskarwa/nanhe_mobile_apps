// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'child_data_from_volunteer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChildDataFromVolunteer _$ChildDataFromVolunteerFromJson(Map json) =>
    ChildDataFromVolunteer(
      id: json['id'] as String?,
      images:
          (json['images'] as List<dynamic>?)?.map((e) => e as String).toList(),
      createdAt: JsonSerializableUtils.timestampToDateTime(
          json['createdAt'] as Timestamp),
      lat: (json['lat'] as num).toDouble(),
      lng: (json['lng'] as num).toDouble(),
      idOfVolunteerWhoUploaded: json['idOfVolunteerWhoUploaded'] as String,
      faceImages: (json['faceImages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$ChildDataFromVolunteerToJson(
        ChildDataFromVolunteer instance) =>
    <String, dynamic>{
      'id': instance.id,
      'images': instance.images,
      'faceImages': instance.faceImages,
      'createdAt':
          JsonSerializableUtils.dateTimeToTimestamp(instance.createdAt),
      'lat': instance.lat,
      'lng': instance.lng,
      'idOfVolunteerWhoUploaded': instance.idOfVolunteerWhoUploaded,
    };
