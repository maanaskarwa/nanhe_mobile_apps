import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

import 'json_serializable_utils.dart';

part 'child_data_from_parent.g.dart';

@JsonSerializable()
class ChildDataFromParent {
  String? matchingId;
  int? matchesFound;
  String uid;
  String? id;
  String name;
  @JsonKey(
      fromJson: JsonSerializableUtils.timestampToDateTime,
      toJson: JsonSerializableUtils.dateTimeToTimestamp
  ) DateTime dob;
  @JsonKey(
      fromJson: JsonSerializableUtils.timestampToDateTime,
      toJson: JsonSerializableUtils.dateTimeToTimestamp
  ) DateTime createdAt;
  @JsonKey(
      fromJson: JsonSerializableUtils.nullableTimestampToDateTime,
      toJson: JsonSerializableUtils.nullableDateTimeToTimestamp)
  DateTime? updatedAt;
  @JsonKey(
      fromJson: JsonSerializableUtils.intToGender,
      toJson: JsonSerializableUtils.genderToInt
  ) Gender gender;
  @JsonKey(
      fromJson: JsonSerializableUtils.timestampToDateTime,
      toJson: JsonSerializableUtils.dateTimeToTimestamp
  ) DateTime missingSince;
  String? identificationMark;
  List<String>? images;
  List<String>? faceImages;
  bool isActive;
  bool found;
  bool matchFound;

  ChildDataFromParent({
    required this.uid,
    this.id,
    required this.name,
    required this.dob,
    required this.gender,
    required this.createdAt,
    this.updatedAt,
    required this.missingSince,
    this.identificationMark,
    this.images,
    this.faceImages,
    this.found = false,
    this.matchFound = false,
    this.isActive = true,
    this.matchesFound,
    this.matchingId,
    }) {
    identificationMark ??= '';
    isActive;
  }

  factory ChildDataFromParent.fromJson(Map<String, dynamic> json) => _$ChildDataFromParentFromJson(json);

  Map<String, dynamic> toJson() => _$ChildDataFromParentToJson(this);

}
