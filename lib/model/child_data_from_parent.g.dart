// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'child_data_from_parent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChildDataFromParent _$ChildDataFromParentFromJson(Map json) =>
    ChildDataFromParent(
      uid: json['uid'] as String,
      id: json['id'] as String?,
      name: json['name'] as String,
      dob: JsonSerializableUtils.timestampToDateTime(json['dob'] as Timestamp),
      gender: JsonSerializableUtils.intToGender(json['gender'] as int),
      createdAt: JsonSerializableUtils.timestampToDateTime(
          json['createdAt'] as Timestamp),
      updatedAt: JsonSerializableUtils.nullableTimestampToDateTime(
          json['updatedAt'] as Timestamp?),
      missingSince: JsonSerializableUtils.timestampToDateTime(
          json['missingSince'] as Timestamp),
      identificationMark: json['identificationMark'] as String?,
      images:
          (json['images'] as List<dynamic>?)?.map((e) => e as String).toList(),
      faceImages: (json['faceImages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      found: json['found'] as bool? ?? false,
      matchFound: json['matchFound'] as bool? ?? false,
      isActive: json['isActive'] as bool? ?? true,
      matchesFound: json['matchesFound'] as int?,
      matchingId: json['matchingId'] as String?,
    );

Map<String, dynamic> _$ChildDataFromParentToJson(
        ChildDataFromParent instance) =>
    <String, dynamic>{
      'matchingId': instance.matchingId,
      'matchesFound': instance.matchesFound,
      'uid': instance.uid,
      'id': instance.id,
      'name': instance.name,
      'dob': JsonSerializableUtils.dateTimeToTimestamp(instance.dob),
      'createdAt':
          JsonSerializableUtils.dateTimeToTimestamp(instance.createdAt),
      'updatedAt':
          JsonSerializableUtils.nullableDateTimeToTimestamp(instance.updatedAt),
      'gender': JsonSerializableUtils.genderToInt(instance.gender),
      'missingSince':
          JsonSerializableUtils.dateTimeToTimestamp(instance.missingSince),
      'identificationMark': instance.identificationMark,
      'images': instance.images,
      'faceImages': instance.faceImages,
      'isActive': instance.isActive,
      'found': instance.found,
      'matchFound': instance.matchFound,
    };
