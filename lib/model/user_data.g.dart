// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserData _$UserDataFromJson(Map json) => UserData(
      uid: json['uid'] as String?,
      name: json['name'] as String,
      email: json['email'] as String,
      signInMethod: json['signInMethod'] as String,
      fcmToken: json['fcmToken'] as String?,
      lat: (json['lat'] as num?)?.toDouble(),
      lng: (json['lng'] as num?)?.toDouble(),
      createdAt: JsonSerializableUtils.timestampToDateTime(
          json['createdAt'] as Timestamp),
      updatedAt: JsonSerializableUtils.nullableTimestampToDateTime(
          json['updatedAt'] as Timestamp?),
      address: json['address'] as String?,
      isParent: json['isParent'] as bool?,
      isVolunteer: json['isVolunteer'] as bool?,
      review: json['review'] as int?,
    );

Map<String, dynamic> _$UserDataToJson(UserData instance) => <String, dynamic>{
      'isParent': instance.isParent,
      'isVolunteer': instance.isVolunteer,
      'uid': instance.uid,
      'name': instance.name,
      'email': instance.email,
      'fcmToken': instance.fcmToken,
      'lat': instance.lat,
      'lng': instance.lng,
      'address': instance.address,
      'review': instance.review,
      'createdAt':
          JsonSerializableUtils.dateTimeToTimestamp(instance.createdAt),
      'updatedAt':
          JsonSerializableUtils.nullableDateTimeToTimestamp(instance.updatedAt),
      'signInMethod': instance.signInMethod,
    };
