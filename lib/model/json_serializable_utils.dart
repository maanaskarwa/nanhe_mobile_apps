import 'package:cloud_firestore/cloud_firestore.dart';

enum Gender { male, female, other }

class JsonSerializableUtils {
  static DateTime? nullableTimestampToDateTime(Timestamp? val) => val != null ? DateTime.fromMillisecondsSinceEpoch(val.millisecondsSinceEpoch) : null;

  static Timestamp? nullableDateTimeToTimestamp(DateTime? time) => time!= null ? Timestamp.fromMillisecondsSinceEpoch(time.millisecondsSinceEpoch) : null;

  static DateTime timestampToDateTime(Timestamp val) => DateTime.fromMillisecondsSinceEpoch(val.millisecondsSinceEpoch);

  static Timestamp dateTimeToTimestamp(DateTime time) => Timestamp.fromMillisecondsSinceEpoch(time.millisecondsSinceEpoch);

  static dynamic dateTimeToTimestampOrServerTime(DateTime time) =>  Timestamp.fromMillisecondsSinceEpoch(time.millisecondsSinceEpoch);

  static int genderToInt(Gender gender) => gender == Gender.male ? 1 : gender == Gender.female ? 2 : 3;

  static Gender intToGender(int gender) => gender == 1 ? Gender.male : gender == 2 ? Gender.female : Gender.other;

}
