import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

import 'json_serializable_utils.dart';

part 'child_data_from_volunteer.g.dart';

@JsonSerializable()
class ChildDataFromVolunteer {
  String? id;
  List<String>? images;
  List<String>? faceImages;
  @JsonKey(
    fromJson: JsonSerializableUtils.timestampToDateTime,
    toJson: JsonSerializableUtils.dateTimeToTimestamp
  ) DateTime createdAt;

  double lat;
  double lng;
  String idOfVolunteerWhoUploaded;

  ChildDataFromVolunteer({
    this.id,
    this.images,
    required this.createdAt,
    required this.lat,
    required this.lng,
    required this.idOfVolunteerWhoUploaded,
    this.faceImages
  }) {
    images ??= [];
  }

  factory ChildDataFromVolunteer.fromJson(Map<String, dynamic> json) => _$ChildDataFromVolunteerFromJson(json);

  Map<String, dynamic> toJson() => _$ChildDataFromVolunteerToJson(this);
}
