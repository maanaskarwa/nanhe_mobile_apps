import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

import 'json_serializable_utils.dart';

part 'user_data.g.dart';

@JsonSerializable()
class UserData {
  bool? isParent;
  bool? isVolunteer;
  String? uid;
  String name;
  String email;
  String? fcmToken;
  double? lat;
  double? lng;
  String? address;

  ///number of stars: null if not reviewed, else number of stars given
  int? review;
  @JsonKey(
    fromJson: JsonSerializableUtils.timestampToDateTime,
    toJson: JsonSerializableUtils.dateTimeToTimestamp
  ) DateTime createdAt;
  @JsonKey(
    fromJson: JsonSerializableUtils.nullableTimestampToDateTime,
    toJson: JsonSerializableUtils.nullableDateTimeToTimestamp
  ) DateTime? updatedAt;
  String signInMethod;
  UserData({
    this.uid,
    required this.name,
    required this.email,
    required this.signInMethod,
    this.fcmToken,
    this.lat,
    this.lng,
    required this.createdAt,
    this.updatedAt,
    this.address,
    this.isParent,
    this.isVolunteer,
    this.review,
  });

  factory UserData.fromJson(Map<String, dynamic> json) => _$UserDataFromJson(json);

  Map<String, dynamic> toJson() => _$UserDataToJson(this);
}
