import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/child_data_from_parent.dart';
import 'screen/Parent/add_child_data_as_parent_screen.dart';
import 'screen/Parent/parent_after_uploading_screen.dart';
import 'screen/Volunteer/add_child_data_as_volunteer.dart';
import 'screen/Volunteer/volunteer_after_uploading_screen.dart';
import 'screen/Volunteer/volunteer_camera_screen.dart';
import 'screen/login_screen_with_email.dart';
import 'screen/login_screen.dart';
import 'screen/master_dashboard_screen.dart';
import 'screen/register_screen.dart';
import 'screen/splash_screen.dart';
import 'screen/subScreen/parent_dashboard.dart';
import 'screen/subScreen/volunteer_dashboard.dart';
import 'screen/tutorial_screen.dart';
import 'util/cameras.dart';

class RoutesDirectory {
  static const String splash = '/';
  static const String login = '/login';
  static const String loginIncludingEmail = '/devLogin';
  static const String register = '/register';
  static const String mainDashboard = '/dashboard';
  static const String parentMain = '/parentMain';
  static const String volunteerMain = '/volunteerMain';
  static const String addChildDataAsVolunteer = '/addChildDataAsVolunteer';
  static const String volunteerAfterUpload = '/volunteerAfterUpload';
  static const String parentAfterUpload = '/parentAfterUpload';
  static const String addChildDataAsParent = '/addChildDataAsParent';
  static const String volunteerCamera = '/volunteer_camera';
  static const String errorScreen = '/error';
  static const String howItWorks = '/howItWorks';
}

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RoutesDirectory.splash:
        if(args is bool) {
          return MaterialPageRoute(builder: (_) => SplashScreen(args));
        }
        return MaterialPageRoute(builder: (_) => const SplashScreen(true));

      case RoutesDirectory.login:
        return MaterialPageRoute(builder: (_) => const LoginScreen());

      case RoutesDirectory.loginIncludingEmail:
        return MaterialPageRoute(builder: (_) => const DevLoginScreen());

      case RoutesDirectory.addChildDataAsVolunteer:
        if(args is Map<String,dynamic>) {
          return MaterialPageRoute(builder: (_) => AddChildDataAsVolunteerScreen(args));
        }
        return _errorRoute();

      case RoutesDirectory.volunteerCamera:
        return MaterialPageRoute(builder: (_) => CameraScreen(cameras: Cameras.appCameras,));

      case RoutesDirectory.volunteerMain:
        return MaterialPageRoute(builder: (_) => const VolunteerDashboard());

      case RoutesDirectory.mainDashboard:
        if(args is int) {
          return MaterialPageRoute(builder: (_) => const MasterDashBoardScreen(1));
        }
        return MaterialPageRoute(builder: (_) => const MasterDashBoardScreen(0));

      case RoutesDirectory.addChildDataAsParent:
        if(args is Map) {
          return MaterialPageRoute(builder: (_) => AddChildDataAsParentScreen(
            currentScreenMode: screenMode.editing,
            previousData: args['previousChildData'] as ChildDataFromParent,
          ));
        }
        return MaterialPageRoute(builder: (_) => const AddChildDataAsParentScreen(currentScreenMode: screenMode.newEntry,));


      case RoutesDirectory.register:
        return MaterialPageRoute(
          builder: (_) => const RegisterScreen(),
        );

      case RoutesDirectory.parentMain:
        return MaterialPageRoute(
          builder: (_) => const ParentDashboard(),
        );

      case RoutesDirectory.howItWorks:
        if(args is bool) {
          return MaterialPageRoute(builder: (_) => NanheIntroductionScreen(args));
        }
        return MaterialPageRoute(builder: (_) => const NanheIntroductionScreen(false));

      case RoutesDirectory.volunteerAfterUpload:
        if(args is String) {
          return MaterialPageRoute(builder: (_) => VolunteerAfterUploadingScreen(url:args));
        }
        return MaterialPageRoute(builder: (_) => const VolunteerAfterUploadingScreen());

      case RoutesDirectory.parentAfterUpload:
        return MaterialPageRoute(builder: (_) => ParentAfterUploadingScreen(args as ChildDataFromParent));

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(),
        body: const Center(
          child: Text('An error occurred. Please restart the app.'),
        ),
      );
    });
  }
}
