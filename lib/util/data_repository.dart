import 'package:nanhe/model/user_data.dart';

class UserDataRepository {
  static late UserData? _userData;

  static void init(UserData userdata) {
    _userData = userdata;
  }

  static UserData? get getUserData {
    if(_userData != null) {
      return _userData!;
    }
    return null;
  }
}


class YouTubeVideoIdRepository {
  static late String? _videoId;

  static void init(String videoId) {
    _videoId = videoId;
  }

  static String get getVideoId {
    if(_videoId != null) {
      return _videoId!;
    }
    return '';
  }
}