import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart' as fb;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:nanhe/bloc/api_repository.dart';
import 'package:nanhe/util/data_repository.dart' show UserDataRepository ;
import 'package:nanhe/widgets/app_loader.dart';
import 'package:nanhe/widgets/app_raised_button.dart';
import 'package:nanhe/widgets/text_view.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../model/user_data.dart';
import '../route_generator.dart' show RoutesDirectory;

class AppUtility {
  static DateTime timestampToDateTime(Timestamp val) =>
      DateTime.fromMillisecondsSinceEpoch(val.millisecondsSinceEpoch);

  static Timestamp dateTimeToTimestamp(DateTime time) =>
      Timestamp.fromMillisecondsSinceEpoch(time.millisecondsSinceEpoch);

  static showProgressDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: AppLoader(),
          );
        });
  }

  static showMessageDialog(
    BuildContext context,
    String message, {
    String title = 'nanhe',
    List<String>? titleArgs,
    List<String>? msgArgs,
    String positiveText = 'Okay',
    bool cancelable = true,
    void Function()? onPressed,
  }) {
    onPressed ??= () {
      Navigator.pop(context);
    };
    showDialog(
      context: context,
      barrierDismissible: cancelable,
      builder: (context) => WillPopScope(
        onWillPop: () async => false,
        child: AlertDialog(
          insetPadding: const EdgeInsets.all(10),
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          content: Container(
            width: double.maxFinite,
            padding: const EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  message,
                ),
                const SizedBox(
                  height: 30,
                ),
                AppRaisedButton(
                  title: positiveText,
                  borderRadius: 30,
                  height: 45,
                  onPressed: onPressed,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  static showConfirmDialog(
      BuildContext context, String message, VoidCallback onPressed,
      {String title = 'nanhe',
      List<String>? titleArgs,
      List<String>? msgArgs,
      String negativeText = 'No',
      String positiveText = 'Yes',
      bool cancelable = true}) {
    showDialog(
      context: context,
      builder: (context) => WillPopScope(
        onWillPop: () async => false,
        child: AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          content: Container(
            width: double.maxFinite,
            padding: const EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextView(
                  title,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 10,
                ),
                TextView(
                  message,
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: AppRaisedButton(
                        title: negativeText,
                        borderRadius: 30,
                        height: 45,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: AppRaisedButton(
                        title: positiveText,
                        borderRadius: 30,
                        height: 45,
                        onPressed: onPressed,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  static void showSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: TextView(
          message,
          textColor: Colors.white,
        ),
      ),
    );
  }

  static Future<DateTime?> getDateFromDatePicker(BuildContext context,
      {DateTime? initialDate, DateTime? firstDate, DateTime? lastDate}) async {
    DateTime currentDate = DateTime.now();
    initialDate = initialDate ?? currentDate;
    firstDate = firstDate ?? DateTime(1900);
    lastDate = lastDate ?? currentDate;
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
    );
    return picked;
  }

  static int? getAge(DateTime dob) =>
      DateTime.now().difference(dob).inDays ~/ 365;

  static Future<bool> enableLocationService() async {
    bool requestService = await Location.instance.requestService();
    print('requestService : $requestService');
    PermissionStatus locationPermissionStatus =
        await Location.instance.requestPermission();
    print('locationPermissionStatus : $locationPermissionStatus');
    bool hasLocationPermissions =
        (locationPermissionStatus == PermissionStatus.granted) ||
            (locationPermissionStatus == PermissionStatus.grantedLimited);
    print('hasLocationPermissions : $hasLocationPermissions');
    return requestService && hasLocationPermissions;
  }

  static Future<XFile?> getImageFromCameraOrGallery(ImageSource source) {
    return ImagePicker().pickImage(source: source, imageQuality: 60);
  }

  static Future googleLogin(BuildContext context) async {
    try {
      GoogleSignIn googleSignIn = GoogleSignIn();
      final GoogleSignInAccount? googleUser = await googleSignIn.signIn();
      if (googleUser == null) {
        return;
      } else {
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;
        final OAuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        showProgressDialog(context);
        final UserCredential _userCredential =
            await FirebaseAuth.instance.signInWithCredential(credential);
        if (_userCredential.additionalUserInfo!.isNewUser) {
          UserData _userData = UserData(
            name: _userCredential.user!.displayName as String,
            email: _userCredential.user!.email as String,
            createdAt: DateTime.now(),
            signInMethod: "GOOGLE",
            uid: _userCredential.user!.uid,
          );
          UserDataRepository.init(_userData);
          APIRepository().incrementVolunteerCount();
          APIRepository().updateUser(_userData);
          AppUtility.showMessageDialog(
            context,
            '''Welcome to the nanhe journey of SAVING CHILDREN 👍

Next time you see a child in begging/labor, just start the app and we'll take you directly to the camera screen so you can upload a picture quickly!!''',
            onPressed: () {
              Navigator.of(context).pushNamedAndRemoveUntil(
                RoutesDirectory.mainDashboard,
                (route) => false,
              );
            },
          );
        } else {
          final userdata =
              await APIRepository().getUserData(_userCredential.user!.uid);
          UserDataRepository.init(userdata!);
          // Navigator.pop(context);
          Navigator.of(context).pushNamedAndRemoveUntil(
              RoutesDirectory.volunteerCamera, (route) => false);
        }
      }
    } catch (e) {
      Navigator.pop(context);
      print(e);
      String errorMessage;
      String error = e.toString();
      print(error);
      if (error.contains("account-exists-with-different-credential")) {
        errorMessage = '''This email is already registered.
Please login using the same authentication as before.''';
      } else if (error.contains("invalid-credential")) {
        errorMessage = """Invalid login. 
Please try again.""";
      } else {
        errorMessage = """Could not log in.
Please try again after 2 minutes.""";
      }
      showMessageDialog(context, errorMessage);
    }
  }

  static Future facebookLogin(BuildContext context) async {
    try {
      final _facebookAuth = fb.FacebookAuth.instance;
      final fb.LoginResult? result = await _facebookAuth.login();
      if (result == null) {
        return;
      } else {
        if (result.status == fb.LoginStatus.success) {
          AuthCredential credential =
              FacebookAuthProvider.credential(result.accessToken!.token);
          showProgressDialog(context);
          final _userCredential =
              await FirebaseAuth.instance.signInWithCredential(credential);
          if (_userCredential.additionalUserInfo!.isNewUser) {
            APIRepository().incrementVolunteerCount();
            UserData _userData = UserData(
              name: _userCredential.user!.displayName as String,
              email: _userCredential.user!.email as String,
              createdAt: DateTime.now(),
              signInMethod: "FACEBOOK",
              uid: _userCredential.user!.uid,
            );
            UserDataRepository.init(_userData);
            APIRepository().updateUser(_userData);
            Navigator.pop(context);
            AppUtility.showMessageDialog(
              context,
              '''Welcome to the nanhe journey of SAVING CHILDREN 👍

Next time you see a child in begging/labor, just start the app and we'll take you directly to the camera screen so you can upload a picture quickly!!''',
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                  RoutesDirectory.mainDashboard,
                  (route) => false,
                );
              },
            );
          } else {
            final UserData? userdata =
                await APIRepository().getUserData(_userCredential.user!.uid);
            if (userdata != null) {
              UserDataRepository.init(userdata);
              Navigator.pop(context);
              Navigator.of(context).pushNamedAndRemoveUntil(
                RoutesDirectory.volunteerCamera,
                (route) => false,
              );
            } else {
              Navigator.pop(context);
              AppUtility.showMessageDialog(context, '''An error occurred.
Please try again later or sign in using a different account.''');
            }
          }
        }
      }
    } catch (e) {
      Navigator.pop(context);
      String errorMessage;
      String error = e.toString();
      if (error.contains("account-exists-with-different-credential")) {
        errorMessage = '''This email is already registered.
Please login using the same authentication as before.''';
      } else if (error.contains("invalid-credential")) {
        errorMessage = """Invalid login.""";
      } else {
        errorMessage = """Could not log in.
Please try again after 2 minutes.""";
      }
      showMessageDialog(context, errorMessage);
    }
  }

  static Future loginWithApple(BuildContext context) async {
    final AuthorizationCredentialAppleID result =
        await SignInWithApple.getAppleIDCredential(scopes: [
      AppleIDAuthorizationScopes.email,
      AppleIDAuthorizationScopes.fullName,
    ]);
    try {
      if (result.userIdentifier != null) {
        OAuthProvider oAuthProvider = OAuthProvider("apple.com");
        final AuthCredential credential = oAuthProvider.credential(
          idToken: result.identityToken,
        );
        final _userCredential =
        await FirebaseAuth.instance.signInWithCredential(credential);
        if (_userCredential.additionalUserInfo!.isNewUser) {
          APIRepository().incrementVolunteerCount();
          UserData _userData = UserData(
            name: _userCredential.user?.displayName??'',
            email: _userCredential.user?.email??'',
            createdAt: DateTime.now(),
            signInMethod: "APPLE",
            uid: _userCredential.user!.uid,
          );
          UserDataRepository.init(_userData);
          APIRepository().updateUser(_userData);
          //Navigator.pop(context);
          AppUtility.showMessageDialog(
            context,
            '''Welcome to the nanhe journey of SAVING CHILDREN 👍

Next time you see a child in begging/labor, just start the app and we'll take you directly to the camera screen so you can upload a picture quickly!!''',
            onPressed: () {
              Navigator.of(context).pushNamedAndRemoveUntil(
                RoutesDirectory.mainDashboard,
                    (route) => false,
              );
            },
          );
        } else {
          final UserData? userdata =
          await APIRepository().getUserData(_userCredential.user!.uid);
          if (userdata != null) {
            UserDataRepository.init(userdata);
            Navigator.pop(context);
            Navigator.of(context).pushNamedAndRemoveUntil(
              RoutesDirectory.volunteerCamera,
                  (route) => false,
            );
          } else {
            Navigator.pop(context);
            AppUtility.showMessageDialog(context, '''An error occurred.
Please try again later or sign in using a different account.''');
          }
        }
      }
    } catch (e) {
      Navigator.pop(context);
      String errorMessage;
      String error = e.toString();
      if (error.contains("account-exists-with-different-credential")) {
        errorMessage = '''This email is already registered.
Please login using the same authentication as before.''';
      } else if (error.contains("invalid-credential")) {
        errorMessage = """Invalid login.""";
      } else {
        errorMessage = """Could not log in.
Please try again after 2 minutes.""";
      }
      showMessageDialog(context, errorMessage);
    }
  }
}
