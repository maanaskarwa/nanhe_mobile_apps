import 'package:camera/camera.dart';

class Cameras {
  static late List<CameraDescription>? appCameras;

  static init(List<CameraDescription> cameras) {
    appCameras = cameras;
  }

  static List<CameraDescription>? getCameraList() {
    if(appCameras != null) {
      return [...appCameras!];
    }
    return null;
  }
}