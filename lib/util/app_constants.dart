class Constants {
  static String appName = 'nanhe';

  static String errorSomethingWentWrong = 'Something went wrong. Please try again.';
  static String dateFormat = 'yyyy-MM-dd';
  static String timeFormat = 'HH:mm';
  static String dateTimeFormat = 'yyyy-MM-dd HH:mm';

  static String displayDateFormat = 'dd MMM yyyy';
  static String displayDateTimeFormat = 'dd MMM yyyy hh:mm aa';
}
