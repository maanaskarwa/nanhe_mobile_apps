import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

import '../model/user_data.dart';
import 'app_constants.dart';


class AuthResponse {
  late bool success;
  UserCredential? firebaseUserCredential;
  UserData? userData;
  String? error;

  AuthResponse({this.success = true, this.firebaseUserCredential, this.userData, this.error});

  factory AuthResponse.fromData(
      {UserCredential? firebaseUserCredential, UserData? userData, dynamic error}) {
    if (error != null) {
      return AuthResponse(
        success: false,
        firebaseUserCredential: firebaseUserCredential,
        userData: userData,
        error: _getError(error),
      );
    } else {
      if (firebaseUserCredential != null && firebaseUserCredential.user != null) {
        return AuthResponse(
          success: true,
          firebaseUserCredential: firebaseUserCredential,
          userData: userData,
          error: _getError(error),
        );
      } else {
        return AuthResponse(
          success: false,
          firebaseUserCredential: firebaseUserCredential,
          userData: userData,
          error: _getError(Constants.errorSomethingWentWrong),
        );
      }
    }
  }

  static String? _getError(dynamic e) {
    if (e == null) {
      return '';
    } else if (e is String) {
      return e;
    } else if (e is PlatformException) {
      return e.message;
    } else if (e is SocketException) {
      return e.message;
    } else if (e is Exception) {
      return e.toString();
    } else if (e is Map) {
      return e.toString();
    }
    return '';
  }
}
